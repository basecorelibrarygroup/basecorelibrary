package com.regalscan.architecture.core.net;

import com.regalscan.architecture.core.net.certificate.TrustAllCerts;
import com.regalscan.architecture.core.net.logger.Logger;

import java.util.concurrent.TimeUnit;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.FlowableTransformer;
import io.reactivex.rxjava3.schedulers.Schedulers;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * 雙重鎖說明參考
 * <a href="https://www.cnblogs.com/xz816111/p/8470048.html">雙重鎖說明參考</a>
 */
public class BaseApi {

    /**
     * 網路設定檔
     */
    private static NetConfig mConfig = null;
    /**
     * Retrofit
     */
    private static Retrofit mRetrofit;
    /**
     * OKHttpClient
     */
    private static OkHttpClient mClient;
    /**
     * 連線逾時預設值
     */
    private static final long DEFAULT_CONNECT_TIMEOUT_MILLS = 60 * 1000L;
    /**
     * 讀取逾時預設值
     */
    private static final long DEFAULT_READ_TIMEOUT_MILLS = 60 * 1000L;
    /**
     * 實例
     */
    private volatile static BaseApi mInstance;
    /**
     * 雙重鎖(DCL, Double Checked Locking)使用的物件
     */
    private static final Object mLock = new Object();

    /**
     * 私有構造, 不讓外部呼叫使用
     */
    private BaseApi() {
        mRetrofit = null;
        mClient = null;
        // 初始化 logger 物件
        Logger.getInstance().init();
    }

    /**
     * 使用雙重鎖 DCL (Double Checked Locking)方式獲取單一實例
     *
     * @return BaseApi
     */
    private static BaseApi getInstance() {
        if (mInstance == null) {
            synchronized (mLock) {
                if (mInstance == null) {
                    mInstance = new BaseApi();
                }
            }
        }

        return mInstance;
    }

    /**
     * 獲取 OKHttpClient
     *
     * @return OKHttpClient
     */
    private OkHttpClient getClient() {
        if (mClient == null) {
            OkHttpClient.Builder builder = new OkHttpClient.Builder();

            // 設定連線逾時時間
            builder.connectTimeout(
                    mConfig.ConnectTimeoutMills() != 0
                            ? mConfig.ConnectTimeoutMills()
                            : DEFAULT_CONNECT_TIMEOUT_MILLS
                    , TimeUnit.MILLISECONDS
            );

            // 設定讀取逾時時間
            builder.readTimeout(
                    mConfig.ReadTimeoutMills() != 0
                            ? mConfig.ReadTimeoutMills()
                            : DEFAULT_READ_TIMEOUT_MILLS
                    , TimeUnit.MILLISECONDS
            );

            // 設定攔截器
            Interceptor[] interceptors = mConfig.Interceptors();
            if (interceptors != null && interceptors.length > 0) {
                for (Interceptor interceptor : interceptors) {
                    builder.addInterceptor(interceptor);
                }
            }

            builder.hostnameVerifier(new TrustAllCerts.TrustAllHostnameVerifier());

            if (mConfig.isLogEnable()) {
                HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
                loggingInterceptor.level(HttpLoggingInterceptor.Level.BODY);
                builder.addInterceptor(loggingInterceptor);

                // 增加紀錄呼叫 WebApi 的位址與參數
                HttpLoggingInterceptor logger = new HttpLoggingInterceptor(s -> Logger.getInstance().writeLog(s));
                logger.level(HttpLoggingInterceptor.Level.BODY);
                builder.addInterceptor(logger);
            }

            mClient = builder.build();
        }

        return mClient;
    }

    /**
     * 獲取 Retrofit
     *
     * @return Retrofit
     */
    private Retrofit getRetrofit() {
        if (mRetrofit == null) {
            Retrofit.Builder builder = new Retrofit.Builder()
                    .baseUrl(mConfig.BaseUrl())
                    .client(getClient());

            builder.addConverterFactory(GsonConverterFactory.create());

            builder.addCallAdapterFactory(RxJava3CallAdapterFactory.create());

            mRetrofit = builder.build();
        }

        return mRetrofit;
    }

    /**
     * 創建 Retrofit
     *
     * @return Retrofit
     */
    @Deprecated
    public static synchronized Retrofit createRetrofit() {
        return getInstance().getRetrofit();
    }

    /**
     * 創建 Service Class
     *
     * @param service Service Class
     * @param <C>     Class 泛型
     * @return Service Class 泛型
     */
    public static synchronized <C> C get(Class<C> service) {
        return getInstance().getRetrofit().create(service);
    }

    /**
     * 註冊設定
     *
     * @param config NetConfig
     */
    public static void registerConfig(NetConfig config) {
        mConfig = config;
        mInstance = null;
    }

    /**
     * 統一線程處理
     *
     * @param <T> 類型
     * @return FlowableTransformer<T, T>
     */
    @Deprecated
    public static <T> FlowableTransformer<T, T> getScheduler() {
        return observable -> observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
