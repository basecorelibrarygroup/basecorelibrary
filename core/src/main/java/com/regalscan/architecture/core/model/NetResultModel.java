package com.regalscan.architecture.core.model;

import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Locale;

public class NetResultModel implements Serializable {
    /**
     * 回傳的狀態碼
     */
//    private int statusCode;
    /**
     * 回傳Message
     */
//    private String message;
    /**
     * 回傳 jsonInfo
     */
    @SerializedName("JsonInfo")
    private Object jsonInfo;
    /**
     * 操作是否成功
     */
//    private boolean result;
    /**
     * Token
     */
    @SerializedName("Token")
    private String token;
    /**
     * 子查詢
     */
    @SerializedName("Data")
    private ResultData data;

    public NetResultModel() {
    }

    public NetResultModel(Object jsonInfo) {
        this.jsonInfo = jsonInfo;
    }

    public NetResultModel(Object jsonInfo, String token) {
        this.jsonInfo = jsonInfo;
        this.token = token;
    }

//    public NetResultModel(int statusCode, String message, Object jsonInfo, boolean result) {
//        this.statusCode = statusCode;
//        this.message = message;
//        this.jsonInfo = jsonInfo;
//        this.result = result;
//    }

//    public int getStatusCode() {
//        return statusCode;
//    }

//    public void setStatusCode(int statusCode) {
//        this.statusCode = statusCode;
//    }

//    public String getMessage() {
//        return message;
//    }

//    public void setMessage(String message) {
//        this.message = message;
//    }

    public Object getJsonInfo() {
        return jsonInfo;
    }

    public void setJsonInfo(Object jsonInfo) {
        this.jsonInfo = jsonInfo;
    }

//    public boolean isResult() {
//        return result;
//    }

//    public void setResult(boolean result) {
//        this.result = result;
//    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public ResultData getData() {
        return data;
    }

    public void setData(ResultData data) {
        this.data = data;
    }

    @NonNull
    @Override
    public String toString() {
//        return "NetResultModel{" +
//                "statusCode=" + statusCode +
//                ", message='" + message + '\'' +
//                ", jsonInfo=" + jsonInfo +
//                ", result=" + result +
//                '}';
        return String.format(Locale.getDefault(),
                "%s{" +
//                        "\"StatusCode\":%d" +
//                        "\"Message\":\"%s\"" +
                        "\"JsonInfo\":\"%s\"" +
//                        "\"Result\":%b" +
                        "\"Token\":\"%s\"" +
                        "\"Data\":\"%s\"" +
                        "}",
                NetResultModel.class.getSimpleName(),
//                statusCode,
//                message,
                jsonInfo,
//                result,
                token,
                data
        );
    }
}
