package com.regalscan.architecture.core.utils;

import android.os.Handler;
import android.widget.EditText;

import androidx.annotation.NonNull;

public class ViewUtils {

    /**
     * 最後一次點擊的時間
     */
    private static long lastClickTime = 0;

    /**
     * 初始化設定點擊間隔的時間
     *
     * @param time 間隔的時間
     */
    public static void setLastClickTime(long time) {
        lastClickTime = time;
    }

    /**
     * 檢測是否短時間內產生連點
     *
     * @return {@code true} 返回後不做後續動作;
     * {@code false} 返回後繼續動作.
     */
    public static boolean isFastDoubleClick() {
        long timeNow = System.currentTimeMillis();
        long timeDiff = timeNow - lastClickTime;
        if (0L < timeDiff && timeDiff < 1000L) {
            return true;
        } else {
            lastClickTime = timeNow;
            return false;
        }
    }

    /**
     * 輸入欄位取得焦點
     *
     * @param editText    輸入欄位
     * @param executeTime delay多少時間去執行
     */
    public static void requestViewFocus(@NonNull EditText editText, long executeTime) {
        if (executeTime == 0) {
            executeTime = 10;
        }

        new Handler().postDelayed(() -> {
            editText.requestFocus();
            editText.selectAll();
        }, executeTime);
    }
}
