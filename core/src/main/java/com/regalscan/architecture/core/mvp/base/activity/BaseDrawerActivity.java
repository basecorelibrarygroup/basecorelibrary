package com.regalscan.architecture.core.mvp.base.activity;

import android.content.Intent;
import android.graphics.Color;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.navigation.NavigationView;
import com.regalscan.architecture.core.R;
import com.regalscan.architecture.core.base.activity.BaseDialogActivity;
import com.regalscan.architecture.core.mvp.presenter.BasePresenter;
import com.regalscan.architecture.core.mvp.view.BaseView;
import com.regalscan.architecture.core.net.exception.NetErrorException;

@Deprecated
public abstract class BaseDrawerActivity<P extends BasePresenter> extends BaseDialogActivity implements BaseView {
    private P mPresenter;

    protected Toolbar mToolbar;
    /**
     * 是否初始化了 Toolbar
     */
    protected boolean isInitToolbar = false;

    // region [ 側邊抽屜 ]
    public int CurrentMenuItem = 0;//紀錄目前User位於哪一個項目
    protected DrawerLayout mDrawer;
    protected FrameLayout mFrame;
    protected NavigationView mNavigation;
    // endregion [ 側邊抽屜 ]

    /**
     * 創建 Presenter
     *
     * @return Presenter 的實例
     */
    protected abstract P createPresenter();

    @Override
    protected void onStart() {
        super.onStart();
        if (!isInitToolbar) {
            initToolbar();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // 銷毀 取消訂閱
        if (mPresenter != null) {
            mPresenter.unDisposable();
        }
    }

    /**
     * 底層獲取 Presenter
     *
     * @return P
     */
    protected synchronized P getPresenter() {
        if (mPresenter == null) {
            mPresenter = createPresenter();
        }
        return mPresenter;
    }

    @Override
    public void onLoading(String message, boolean cancelable) {
        showLoadingDialog(message, cancelable);
    }

    @Override
    public void onMessage(String message) {
        showMessageDialog(message);
    }

    @Override
    public void onError(String message) {
        showErrorDialog(message);
    }

    @Override
    public void onError(Exception exception) {
        showErrorDialog(exception.getMessage());
    }

    @Override
    public void onError(NetErrorException exception) {
        showErrorDialog(exception.getMessage());
    }

//    @Override
//    public void onFailure(String message) {
//        showWarningDialog(message);
//    }
//
//    @Override
//    public void onFailure(NetErrorException exception) {
//        showWarningDialog(exception.getMessage());
//    }

    @Override
    public void onComplete() {
        // 請求完成，關閉 Dialog
        onDismissDialog(0);
    }

    @Override
    public void onDialogCancelListener(AlertDialog dialog) {
        super.onDialogCancelListener(dialog);
        // Dialog 取消，取消訂閱
        if (mPresenter != null) {
            mPresenter.unDisposable();
        }
    }

    /**
     * @param layoutResID 資源 ID
     * @param initNav     設定畫面布局是否啟用 Navigation
     */
    public void setContentView(int layoutResID, boolean initNav) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        if (!initNav) {
            super.setContentView(layoutResID);
        } else {
            mDrawer = (DrawerLayout) getLayoutInflater().inflate(R.layout.navi_drawer, findViewById(R.id.drawer_layout));
            mFrame = mDrawer.findViewById(R.id.content_frame);
            mNavigation = mDrawer.findViewById(R.id.nav_view);
            getLayoutInflater().inflate(layoutResID, mFrame, true);

            super.setContentView(mDrawer);

            initNavigationView();
        }
    }

    private void initNavigationView() {
        mNavigation.setNavigationItemSelectedListener(menuItem -> {
            if (menuItem != mNavigation.getMenu().getItem(CurrentMenuItem)) {
                int id = menuItem.getItemId();
                if (id == R.id.action_desktop) {
                    try {
                        startActivity(new Intent(mActivity, Class.forName("com.regalscan.frame.module_app.desktop.MenuActivity")));
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                } else if (id == R.id.action_setting) {
                    try {
                        startActivity(new Intent(mActivity, Class.forName("com.regalscan.frame.module_app.settings.SettingActivity")));
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                } else if (id == R.id.action_setting_in) {
                    try {
                        startActivity(new Intent(mActivity, Class.forName("com.regalscan.frame.module_app.desktop.SettingInActivity")));
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                } else if (id == R.id.action_about) {
                    try {
                        startActivity(new Intent(mActivity, Class.forName("com.regalscan.frame.module_app.desktop.AboutActivity")));
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                } else if (id == R.id.action_logout) {
                    exitSystem();
                }
            }
            mDrawer.closeDrawer(GravityCompat.START);
            return false;
        });
    }

    protected void initToolbar() {
        initToolbar(R.id.base_toolbar);
    }

    protected void initToolbar(int resId) {
        initToolbar(resId, "");
    }

    protected void initToolbar(int resId, String title) {
        mToolbar = findViewById(resId);
        if (mToolbar != null) {
            // 清除標題
            mToolbar.setTitle(title);
            // 設置為透明色
            mToolbar.setBackgroundColor(Color.TRANSPARENT);
            // 設置為全透明
            mToolbar.getBackground().setAlpha(0);
            setSupportActionBar(mToolbar);

            mToolbar.setNavigationOnClickListener(view -> mDrawer.closeDrawer(GravityCompat.START));

            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(mActivity, mDrawer, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
                @Override
                public void onDrawerOpened(View drawerView) {
                    super.onDrawerOpened(drawerView);
                }

                @Override
                public void onDrawerClosed(View drawerView) {
                    super.onDrawerClosed(drawerView);
                }
            };
            mDrawer.addDrawerListener(toggle);
            toggle.syncState();

            isInitToolbar = true;
        }
    }

    public void exitSystem() {
        new AlertDialog.Builder(mActivity)
                .setTitle("確定要登出 ?")
                .setMessage("登出後將會關閉所有作業 !")
                .setCancelable(false)
                .setPositiveButton(getString(R.string.core_btn_confirm), (dialog, which) -> {
                    finish();
                })
                .setNeutralButton(getString(R.string.core_btn_cancel), (dialog, which) -> dialog.dismiss())
                .show();
    }
}
