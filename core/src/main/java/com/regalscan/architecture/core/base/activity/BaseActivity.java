package com.regalscan.architecture.core.base.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.Nullable;

import com.regalscan.architecture.core.R;
import com.trello.rxlifecycle4.components.support.RxAppCompatActivity;

public class BaseActivity extends RxAppCompatActivity {
    protected Activity mActivity;
    protected Handler mHandler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = this;
        mHandler = new Handler();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    /**
     * 獲取狀態欄的高度
     *
     * @param context {@link Context}
     * @return 狀態欄高度
     */
    @Deprecated
    private int getStatusBarHeight(Context context) {
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        return context.getResources().getDimensionPixelSize(resourceId);
    }

    /**
     * 往左邊滑出
     */
    protected void slideLeftOut() {
        mActivity.overridePendingTransition(R.anim.setup_pre_in, R.anim.setup_pre_out);
    }

    /**
     * 從右邊滑入
     */
    protected void slideRightIn() {
        mActivity.overridePendingTransition(R.anim.setup_next_in, R.anim.setup_next_out);
    }

    @Override
    public void finish() {
        super.finish();
        slideLeftOut();
    }
}
