package com.regalscan.architecture.core.component.dialog;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.regalscan.architecture.core.R;

public class DialogHelper implements DialogInterface.OnCancelListener, DialogInterface.OnKeyListener {

    // region [ 物件宣告 ]
    /**
     * 持有 Activity
     */
    private final Activity mActivity;
    /**
     * AlertDialog 對象
     */
    private AlertDialog mDialog;
    /**
     * 彈窗的樣式
     */
    private final int mStyle;
    /**
     * 布局解析器
     */
    private final LayoutInflater mInflater;
    /**
     * 取消彈窗的回調監聽
     */
    private final OnDialogCancelListener mDialogCancelListener;
    /**
     * 按鍵 {@link DialogInterface.OnKeyListener} 的回調監聽
     */
    private final OnDialogKeyListener mDialogKeyListener;
    /**
     * 帶進度條的 Dialog {@link ProgressDialog}
     */
    private ProgressDialog mProgressDialog;
    // endregion [ 物件宣告 ]

    // region [ 物件初始化 ]
    // region [ 使用 Activity 初始化 ]

    /**
     * 不傳遞樣式, 使用默認樣式
     *
     * @param activity               {@link Activity}
     * @param onDialogCancelListener {@link OnDialogCancelListener}
     */
    public DialogHelper(Activity activity, OnDialogCancelListener onDialogCancelListener) {
        this(activity, R.style.AppAlertDialogStyle, onDialogCancelListener, null);
    }

    /**
     * 不傳遞樣式, 使用默認樣式
     *
     * @param activity               {@link Activity}
     * @param onDialogCancelListener {@link OnDialogCancelListener}
     * @param onDialogKeyListener    {@link OnDialogKeyListener}
     */
    public DialogHelper(Activity activity, OnDialogCancelListener onDialogCancelListener, OnDialogKeyListener onDialogKeyListener) {
        this(activity, R.style.AppAlertDialogStyle, onDialogCancelListener, onDialogKeyListener);
    }

    /**
     * 傳遞樣式
     *
     * @param activity               {@link Activity}
     * @param dialogStyle            彈窗樣式
     * @param onDialogCancelListener {@link OnDialogCancelListener}
     * @param onDialogKeyListener    {@link OnDialogKeyListener}
     */
    public DialogHelper(Activity activity, int dialogStyle, OnDialogCancelListener onDialogCancelListener, OnDialogKeyListener onDialogKeyListener) {
        mActivity = activity;
        mStyle = dialogStyle;
        mInflater = LayoutInflater.from(mActivity);
        mDialogCancelListener = onDialogCancelListener;
        mDialogKeyListener = onDialogKeyListener;

        initProgressDialog();
    }
    // endregion [ 使用 Activity 初始化 ]

    // region [ 使用 Fragment 初始化 ]

    /**
     * 不傳遞樣式, 使用默認樣式
     *
     * @param fragment               {@link Fragment}
     * @param onDialogCancelListener {@link OnDialogCancelListener}
     */
    public DialogHelper(Fragment fragment, OnDialogCancelListener onDialogCancelListener) {
        this(fragment, R.style.AppAlertDialogStyle, onDialogCancelListener, null);
    }

    /**
     * 不傳遞樣式, 使用默認樣式
     *
     * @param fragment               {@link Fragment}
     * @param onDialogCancelListener {@link OnDialogCancelListener}
     * @param onDialogKeyListener    {@link OnDialogKeyListener}
     */
    public DialogHelper(Fragment fragment, OnDialogCancelListener onDialogCancelListener, OnDialogKeyListener onDialogKeyListener) {
        this(fragment, R.style.AppAlertDialogStyle, onDialogCancelListener, onDialogKeyListener);
    }

    /**
     * 傳遞樣式
     *
     * @param fragment               {@link Fragment}
     * @param dialogStyle            彈窗樣式
     * @param onDialogCancelListener {@link OnDialogCancelListener}
     * @param onDialogKeyListener    {@link OnDialogKeyListener}
     */
    public DialogHelper(Fragment fragment, int dialogStyle, OnDialogCancelListener onDialogCancelListener, OnDialogKeyListener onDialogKeyListener) {
        mActivity = fragment.requireActivity();
        mStyle = dialogStyle;
        mInflater = LayoutInflater.from(mActivity);
        mDialogCancelListener = onDialogCancelListener;
        mDialogKeyListener = onDialogKeyListener;

        initProgressDialog();
    }
    // endregion [ 使用 Fragment 初始化 ]
    // endregion [ 物件初始化 ]

    // region [ android 內建的帶進度條 Dialog ]

    /**
     * 初始化 android 內建的進度條 Dialog
     */
    private void initProgressDialog() {
        mProgressDialog = new ProgressDialog(mActivity);
    }

    /**
     * 顯示訊息
     *
     * @param resId 傳入 Resource Id 型別的字串
     */
    public void setMessage(@StringRes int resId) {
        setMessage(mActivity.getString(resId));
    }

    /**
     * 顯示訊息
     *
     * @param resId      傳入 Resource Id 型別的字串
     * @param cancelable 能不能點擊空白的地方來關閉視窗
     */
    public void setMessage(@StringRes int resId, boolean cancelable) {
        setMessage(mActivity.getString(resId), cancelable);
    }

    /**
     * 顯示訊息
     *
     * @param msg 傳入字串
     */
    public void setMessage(String msg) {
        setMessage(msg, false);
    }

    /**
     * 顯示訊息
     *
     * @param msg        傳入字串
     * @param cancelable 能不能點擊空白的地方來關閉視窗
     */
    public void setMessage(String msg, boolean cancelable) {
        mProgressDialog.setCancelable(cancelable);
        mProgressDialog.setMessage(msg);
        if (mProgressDialog != null && !mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }
    // endregion [ android 內建的帶進度條 Dialog ]

    // region [ Loading 視窗 ]

    /**
     * 顯示 loading 彈窗, 默認不能點空白處進行取消
     *
     * @param resId loading 彈窗的訊息文字
     */
    public void showLoadingDialog(@StringRes int resId) {
        showLoadingDialog(mActivity.getString(resId));
    }

    /**
     * 顯示 loading 彈窗
     *
     * @param resId      loading 彈窗的訊息文字
     * @param cancelable 能不能點擊空白的地方來關閉視窗
     */
    public void showLoadingDialog(@StringRes int resId, boolean cancelable) {
        showLoadingDialog(mActivity.getString(resId), cancelable);
    }

    /**
     * 顯示 loading 彈窗, 默認不能點空白處進行取消
     *
     * @param loadingTip loading 彈窗的訊息文字
     */
    public void showLoadingDialog(String loadingTip) {
        showLoadingDialog(loadingTip, false);
    }

    /**
     * 顯示 loading 彈窗
     *
     * @param loadingTip loading 彈窗的訊息文字
     * @param cancelable 能不能點擊空白的地方來關閉視窗
     */
    public void showLoadingDialog(String loadingTip, boolean cancelable) {
        // 解析布局
        View mDialogView = mInflater.inflate(R.layout.layout_dialog_loading, null);
        // 消息
        mDialogView.<TextView>findViewById(R.id.txt_dialog_loading).setText(loadingTip);
        createAndShowDialog(mDialogView, cancelable, null);
    }
    // endregion [ Loading 視窗 ]
    // region [ Message 視窗 ]

    /**
     * 訊息提示彈窗
     *
     * @param resId 提示訊息的內容
     */
    public void showMessageDialog(@StringRes int resId) {
        showMessageDialog(mActivity.getString(resId));
    }

    /**
     * 訊息提示彈窗
     *
     * @param resId           提示訊息的內容
     * @param confirmListener 確認按鈕點擊的回調
     */
    public void showMessageDialog(@StringRes int resId, OnDialogConfirmListener confirmListener) {
        showMessageDialog(mActivity.getString(resId), confirmListener, null);
    }

    /**
     * 訊息提示彈窗
     *
     * @param resId           提示訊息的內容
     * @param confirmListener 確認按鈕點擊的回調
     * @param keyListener     監聽按鍵的回調
     */
    public void showMessageDialog(@StringRes int resId, OnDialogConfirmListener confirmListener, DialogInterface.OnKeyListener keyListener) {
        showMessageDialog(mActivity.getString(resId), confirmListener, keyListener);
    }

    /**
     * 訊息提示彈窗
     *
     * @param message 提示訊息的內容
     */
    public void showMessageDialog(String message) {
        showMessageDialog(message, null);
    }

    /**
     * 訊息提示彈窗
     *
     * @param message         提示訊息的內容
     * @param confirmListener 確認按鈕點擊的回調
     */
    public void showMessageDialog(String message, OnDialogConfirmListener confirmListener) {
        showMessageDialog(message, confirmListener, null);
    }

    /**
     * 訊息提示彈窗
     *
     * @param message         提示訊息的內容
     * @param confirmListener 確認按鈕點擊的回調
     * @param keyListener     監聽按鍵的回調
     */
    public void showMessageDialog(String message, OnDialogConfirmListener confirmListener, DialogInterface.OnKeyListener keyListener) {
        createHasIconDialog(-1, message, confirmListener, keyListener);
    }
    // endregion [ Message 視窗 ]
    // region [ Success 視窗 ]

    /**
     * 成功提示彈窗
     *
     * @param resId 提示訊息的內容
     */
    public void showSuccessDialog(@StringRes int resId) {
        showSuccessDialog(mActivity.getString(resId), null);
    }

    /**
     * 成功提示彈窗
     *
     * @param resId           提示訊息的內容
     * @param confirmListener 確認按鈕點擊的回調
     */
    public void showSuccessDialog(@StringRes int resId, OnDialogConfirmListener confirmListener) {
        showSuccessDialog(mActivity.getString(resId), confirmListener, null);
    }

    /**
     * 成功提示彈窗
     *
     * @param resId           提示訊息的內容
     * @param confirmListener 確認按鈕點擊的回調
     * @param keyListener     監聽按鍵的回調
     */
    public void showSuccessDialog(@StringRes int resId, OnDialogConfirmListener confirmListener, DialogInterface.OnKeyListener keyListener) {
        showSuccessDialog(mActivity.getString(resId), confirmListener, keyListener);
    }

    /**
     * 成功提示彈窗
     *
     * @param message 提示訊息的內容
     */
    public void showSuccessDialog(String message) {
        showSuccessDialog(message, null);
    }

    /**
     * 成功提示彈窗
     *
     * @param message         提示訊息的內容
     * @param confirmListener 確認按鈕點擊的回調
     */
    public void showSuccessDialog(String message, OnDialogConfirmListener confirmListener) {
        showSuccessDialog(message, confirmListener, null);
    }

    /**
     * 成功提示彈窗
     *
     * @param message         提示訊息的內容
     * @param confirmListener 確認按鈕點擊的回調
     */
    public void showSuccessDialog(String message, OnDialogConfirmListener confirmListener, DialogInterface.OnKeyListener keyListener) {
        createHasIconDialog(R.mipmap.icon_dialog_success, message, confirmListener, keyListener);
    }
    // endregion [ Success 視窗 ]
    // region [ Warning 視窗 ]

    /**
     * 警告提示彈窗
     *
     * @param resId 提示訊息的內容
     */
    public void showWarningDialog(@StringRes int resId) {
        showWarningDialog(mActivity.getString(resId), null);
    }

    /**
     * 警告提示彈窗
     *
     * @param resId           提示訊息的內容
     * @param confirmListener 確認按鈕點擊的回調
     */
    public void showWarningDialog(@StringRes int resId, OnDialogConfirmListener confirmListener) {
        showWarningDialog(mActivity.getString(resId), confirmListener, null);
    }

    /**
     * 警告提示彈窗
     *
     * @param resId           提示訊息的內容
     * @param confirmListener 確認按鈕點擊的回調
     * @param keyListener     監聽按鍵的回調
     */
    public void showWarningDialog(@StringRes int resId, OnDialogConfirmListener confirmListener, DialogInterface.OnKeyListener keyListener) {
        showWarningDialog(mActivity.getString(resId), confirmListener, keyListener);
    }

    /**
     * 警告提示彈窗
     *
     * @param message 提示訊息的內容
     */
    public void showWarningDialog(String message) {
        showWarningDialog(message, null);
    }

    /**
     * 警告提示彈窗
     *
     * @param message         提示訊息的內容
     * @param confirmListener 確認按鈕點擊的回調
     */
    public void showWarningDialog(String message, OnDialogConfirmListener confirmListener) {
        showWarningDialog(message, confirmListener, null);
    }

    /**
     * 警告提示彈窗
     *
     * @param message         提示訊息的內容
     * @param confirmListener 確認按鈕點擊的回調
     * @param keyListener     監聽按鍵的回調
     */
    public void showWarningDialog(String message, OnDialogConfirmListener confirmListener, DialogInterface.OnKeyListener keyListener) {
        createHasIconDialog(R.mipmap.icon_dialog_warning, message, confirmListener, keyListener);
    }
    // endregion [ Warning 視窗 ]
    // region [ Error 視窗 ]

    /**
     * 錯誤提示彈窗
     *
     * @param resId 提示訊息的內容
     */
    public void showErrorDialog(@StringRes int resId) {
        showErrorDialog(mActivity.getString(resId), null);
    }

    /**
     * 錯誤提示彈窗
     *
     * @param resId           提示訊息的內容
     * @param confirmListener 確認按鈕點擊的回調
     */
    public void showErrorDialog(@StringRes int resId, OnDialogConfirmListener confirmListener) {
        showErrorDialog(mActivity.getString(resId), confirmListener, null);
    }

    /**
     * 錯誤提示彈窗
     *
     * @param resId           提示訊息的內容
     * @param confirmListener 確認按鈕點擊的回調
     * @param keyListener     監聽按鍵的回調
     */
    public void showErrorDialog(@StringRes int resId, OnDialogConfirmListener confirmListener, DialogInterface.OnKeyListener keyListener) {
        showErrorDialog(mActivity.getString(resId), confirmListener, keyListener);
    }

    /**
     * 錯誤提示彈窗
     *
     * @param message 提示訊息的內容
     */
    public void showErrorDialog(String message) {
        showErrorDialog(message, null);
    }

    /**
     * 錯誤提示彈窗
     *
     * @param message         提示訊息的內容
     * @param confirmListener 確認按鈕點擊的回調
     */
    public void showErrorDialog(String message, OnDialogConfirmListener confirmListener) {
        showErrorDialog(message, confirmListener, null);
    }

    /**
     * 錯誤提示彈窗
     *
     * @param message         提示訊息的內容
     * @param confirmListener 確認按鈕點擊的回調
     * @param keyListener     監聽按鍵的回調
     */
    public void showErrorDialog(String message, OnDialogConfirmListener confirmListener, DialogInterface.OnKeyListener keyListener) {
        createHasIconDialog(R.mipmap.icon_dialog_error, message, confirmListener, keyListener);
    }
    // endregion [ Error 視窗 ]
    // region [ Confirm 視窗 ]

    /**
     * 顯示確認彈窗
     *
     * @param messageId       提示訊息的內容
     * @param confirmListener 確認按鈕點擊回調
     */
    public void showConfirmDialog(@StringRes int messageId, OnDialogConfirmListener confirmListener) {
        showConfirmDialog(mActivity.getString(messageId), mActivity.getString(R.string.core_btn_confirm), mActivity.getString(R.string.core_btn_cancel), confirmListener);
    }

    /**
     * 顯示確認彈窗
     *
     * @param messageId       提示訊息的內容
     * @param confirmId       確認按鈕文字
     * @param cancelId        取消按鈕文字
     * @param confirmListener 確認按鈕點擊回調
     */
    public void showConfirmDialog(@StringRes int messageId,
                                  @StringRes int confirmId,
                                  @StringRes int cancelId,
                                  OnDialogConfirmListener confirmListener) {
        showConfirmDialog(mActivity.getString(messageId), mActivity.getString(confirmId), mActivity.getString(cancelId), confirmListener, null);
    }

    /**
     * 顯示確認彈窗
     *
     * @param messageId       提示訊息的內容
     * @param confirmId       確認按鈕文字
     * @param cancelId        取消按鈕文字
     * @param confirmListener 確認按鈕點擊回調
     * @param cancelListener  取消按鈕點擊回調
     */
    public void showConfirmDialog(@StringRes int messageId,
                                  @StringRes int confirmId,
                                  @StringRes int cancelId,
                                  final OnDialogConfirmListener confirmListener,
                                  final OnDialogCancelListener cancelListener) {
        showConfirmDialog(mActivity.getString(messageId), mActivity.getString(confirmId), mActivity.getString(cancelId), confirmListener, cancelListener, null);
    }

    /**
     * 顯示確認彈窗
     *
     * @param messageId       提示訊息的內容
     * @param confirmId       確認按鈕文字
     * @param cancelId        取消按鈕文字
     * @param confirmListener 確認按鈕點擊回調
     * @param cancelListener  取消按鈕點擊回調
     * @param onKeyListener   監聽按鍵的回調
     */
    public void showConfirmDialog(@StringRes int messageId,
                                  @StringRes int confirmId,
                                  @StringRes int cancelId,
                                  final OnDialogConfirmListener confirmListener,
                                  final OnDialogCancelListener cancelListener,
                                  final DialogInterface.OnKeyListener onKeyListener) {
        showConfirmDialog(mActivity.getString(messageId), mActivity.getString(confirmId), mActivity.getString(cancelId), confirmListener, cancelListener, onKeyListener);
    }

    /**
     * 顯示確認彈窗
     *
     * @param message         提示訊息的內容
     * @param confirmListener 確認按鈕點擊回調
     */
    public void showConfirmDialog(String message, OnDialogConfirmListener confirmListener) {
        showConfirmDialog(message, mActivity.getString(R.string.core_btn_confirm), mActivity.getString(R.string.core_btn_cancel), confirmListener);
    }

    /**
     * 顯示確認彈窗
     *
     * @param message         提示訊息的內容
     * @param confirmText     確認按鈕文字
     * @param cancelText      取消按鈕文字
     * @param confirmListener 確認按鈕點擊回調
     */
    public void showConfirmDialog(String message,
                                  String confirmText,
                                  String cancelText,
                                  OnDialogConfirmListener confirmListener) {
        showConfirmDialog(message, confirmText, cancelText, confirmListener, null);
    }

    /**
     * 顯示確認彈窗
     *
     * @param message         提示訊息的內容
     * @param confirmText     確認按鈕文字
     * @param cancelText      取消按鈕文字
     * @param confirmListener 確認按鈕點擊回調
     * @param cancelListener  取消按鈕點擊回調
     */
    public void showConfirmDialog(String message,
                                  String confirmText,
                                  String cancelText,
                                  final OnDialogConfirmListener confirmListener,
                                  final OnDialogCancelListener cancelListener) {
        showConfirmDialog(message, confirmText, cancelText, confirmListener, cancelListener, null);
    }

    /**
     * 顯示確認彈窗
     *
     * @param message         提示訊息的內容
     * @param confirmText     確認按鈕文字
     * @param cancelText      取消按鈕文字
     * @param confirmListener 確認按鈕點擊的回調
     * @param cancelListener  取消按鈕點擊回調
     * @param onKeyListener   監聽按鍵的回調
     */
    public void showConfirmDialog(String message,
                                  String confirmText,
                                  String cancelText,
                                  final OnDialogConfirmListener confirmListener,
                                  final OnDialogCancelListener cancelListener,
                                  final DialogInterface.OnKeyListener onKeyListener) {
        // 解析布局
        View mDialogView = mInflater.inflate(R.layout.layout_dialog_confirm, null);
        // 消息
        mDialogView.<TextView>findViewById(R.id.txt_dialog_message).setText(message);

        // 確認按鈕
        Button confirmButton = mDialogView.findViewById(R.id.btn_confirm);
        initActionButton(confirmButton, confirmText, v -> {
            if (confirmListener != null) {
                confirmListener.onDialogConfirmListener(mDialog);
            }
        });

        // 取消按鈕
        Button cancelButton = mDialogView.findViewById(R.id.btn_cancel);
        initActionButton(cancelButton, cancelText, v -> {
            if (cancelListener != null) {
                cancelListener.onDialogCancelListener(mDialog);
            }
        });

        // 創建和顯示彈窗
        createAndShowDialog(mDialogView, false, onKeyListener);
    }
    // endregion [ Confirm 視窗 ]
    // region [ 密碼輸入視窗 ]

    /**
     * 顯示密碼輸入彈窗
     *
     * @param messageId       標題文字
     * @param confirmListener 確認按鈕點擊回調
     */
    public void showPasswordVerifyDialog(@StringRes int messageId, OnPasswordVerifyDialogConfirmListener confirmListener) {
        showPasswordVerifyDialog(mActivity.getString(messageId), mActivity.getString(R.string.core_btn_confirm), mActivity.getString(R.string.core_btn_cancel), confirmListener);
    }

    /**
     * 顯示密碼輸入彈窗
     *
     * @param messageId       標題文字
     * @param confirmId       確認按鈕文字
     * @param cancelId        取消按鈕文字
     * @param confirmListener 確認按鈕點擊回調
     */
    public void showPasswordVerifyDialog(@StringRes int messageId,
                                         @StringRes int confirmId,
                                         @StringRes int cancelId,
                                         final OnPasswordVerifyDialogConfirmListener confirmListener) {
        showPasswordVerifyDialog(mActivity.getString(messageId), mActivity.getString(confirmId), mActivity.getString(cancelId), confirmListener, null);
    }

    /**
     * 顯示密碼輸入彈窗
     *
     * @param messageId       標題文字
     * @param confirmId       確認按鈕文字
     * @param cancelId        取消按鈕文字
     * @param confirmListener 確認按鈕點擊回調
     * @param cancelListener  取消按鈕點擊回調
     */
    public void showPasswordVerifyDialog(@StringRes int messageId,
                                         @StringRes int confirmId,
                                         @StringRes int cancelId,
                                         final OnPasswordVerifyDialogConfirmListener confirmListener,
                                         final OnDialogCancelListener cancelListener) {
        showPasswordVerifyDialog(mActivity.getString(messageId), mActivity.getString(confirmId), mActivity.getString(cancelId), confirmListener, cancelListener, null);
    }

    /**
     * 顯示密碼輸入彈窗
     *
     * @param messageId       標題文字
     * @param confirmId       確認按鈕文字
     * @param cancelId        取消按鈕文字
     * @param confirmListener 確認按鈕點擊回調
     * @param cancelListener  取消按鈕點擊回調
     * @param onKeyListener   監聽按鍵的回調
     */
    public void showPasswordVerifyDialog(@StringRes int messageId,
                                         @StringRes int confirmId,
                                         @StringRes int cancelId,
                                         final OnPasswordVerifyDialogConfirmListener confirmListener,
                                         final OnDialogCancelListener cancelListener,
                                         final DialogInterface.OnKeyListener onKeyListener) {
        showPasswordVerifyDialog(mActivity.getString(messageId), mActivity.getString(confirmId), mActivity.getString(cancelId), confirmListener, cancelListener, onKeyListener);
    }

    /**
     * 顯示密碼輸入彈窗
     *
     * @param header          標題文字
     * @param confirmListener 確認按鈕點擊回調
     */
    public void showPasswordVerifyDialog(String header, OnPasswordVerifyDialogConfirmListener confirmListener) {
        showPasswordVerifyDialog(header, mActivity.getString(R.string.core_btn_confirm), mActivity.getString(R.string.core_btn_cancel), confirmListener);
    }

    /**
     * 顯示密碼輸入彈窗
     *
     * @param header          標題文字
     * @param confirmText     確認按鈕文字
     * @param confirmListener 確認按鈕點擊回調
     */
    public void showPasswordVerifyDialog(String header,
                                         String confirmText,
                                         String cancelText,
                                         final OnPasswordVerifyDialogConfirmListener confirmListener) {
        showPasswordVerifyDialog(header, confirmText, cancelText, confirmListener, null);
    }

    /**
     * 顯示密碼輸入彈窗
     *
     * @param header          標題文字
     * @param confirmText     確認按鈕文字
     * @param cancelText      取消按鈕文字
     * @param confirmListener 確認按鈕點擊回調
     * @param cancelListener  取消按鈕點擊回調
     */
    public void showPasswordVerifyDialog(String header,
                                         String confirmText,
                                         String cancelText,
                                         final OnPasswordVerifyDialogConfirmListener confirmListener,
                                         final OnDialogCancelListener cancelListener) {
        showPasswordVerifyDialog(header, confirmText, cancelText, confirmListener, cancelListener, null);
    }

    /**
     * 顯示密碼輸入彈窗
     *
     * @param header          標題文字
     * @param confirmText     確認按鈕文字
     * @param cancelText      取消按鈕文字
     * @param confirmListener 確認按鈕點擊回調
     * @param cancelListener  取消按鈕點擊回調
     * @param onKeyListener   監聽按鍵的回調
     */
    public void showPasswordVerifyDialog(String header,
                                         String confirmText,
                                         String cancelText,
                                         final OnPasswordVerifyDialogConfirmListener confirmListener,
                                         final OnDialogCancelListener cancelListener,
                                         final DialogInterface.OnKeyListener onKeyListener) {
        // 解析布局
        View mDialogView = mInflater.inflate(R.layout.layout_dialog_password_verify, null);
        // 標題
        mDialogView.<TextView>findViewById(R.id.txt_dialog_header).setText(header);

        // 輸入欄位
        EditText mEditText = mDialogView.findViewById(R.id.txt_dialog_message);
        mEditText.requestFocus();

        mDialogView.<ToggleButton>findViewById(R.id.btn_dialog_pwd_toggle)
                .setOnCheckedChangeListener((buttonView, isChecked) -> {
                    mEditText.setTransformationMethod(isChecked
                            ? HideReturnsTransformationMethod.getInstance()
                            : PasswordTransformationMethod.getInstance()
                    );
                    mEditText.setSelection(mEditText.getText().length());
                });

        // 確認按鈕
        Button confirmButton = mDialogView.findViewById(R.id.btn_confirm);
        initActionButton(confirmButton, confirmText, v -> {
            if (confirmListener != null) {
                confirmListener.onDialogConfirmListener(mDialog, mEditText.getText());
            }
        });

        // 取消按鈕
        Button cancelButton = mDialogView.findViewById(R.id.btn_cancel);
        if ("".equals(cancelText)) {
            cancelButton.setVisibility(View.GONE);
        } else {
            initActionButton(cancelButton, cancelText, v -> {
                if (cancelListener != null) {
                    cancelListener.onDialogCancelListener(mDialog);
                }
            });
        }

        // 創建和顯示彈窗
        createAndShowDialog(mDialogView, false, onKeyListener);
    }
    // endregion [ 密碼輸入視窗 ]
    // region [ 文字輸入視窗 ]

    /**
     * 顯示文字輸入彈窗
     *
     * @param messageId       標題文字
     * @param confirmListener 確認按鈕點擊回調
     */
    public void showTextInputDialog(@StringRes int messageId, OnTextInputDialogConfirmListener confirmListener) {
        showTextInputDialog(mActivity.getString(messageId), mActivity.getString(R.string.core_btn_confirm), confirmListener);
    }

    /**
     * 顯示文字輸入彈窗
     *
     * @param messageId       標題文字
     * @param confirmId       確認按鈕文字
     * @param confirmListener 確認按鈕點擊回調
     */
    public void showTextInputDialog(@StringRes int messageId,
                                    @StringRes int confirmId,
                                    final OnTextInputDialogConfirmListener confirmListener) {
        showTextInputDialog(mActivity.getString(messageId), mActivity.getString(confirmId), confirmListener, null);
    }

    /**
     * 顯示文字輸入彈窗
     *
     * @param messageId       標題文字
     * @param confirmId       確認按鈕文字
     * @param confirmListener 確認按鈕點擊回調
     * @param onKeyListener   監聽按鍵的回調
     */
    public void showTextInputDialog(@StringRes int messageId,
                                    @StringRes int confirmId,
                                    final OnTextInputDialogConfirmListener confirmListener,
                                    final DialogInterface.OnKeyListener onKeyListener) {
        showTextInputDialog(mActivity.getString(messageId), mActivity.getString(confirmId), confirmListener, onKeyListener);
    }

    /**
     * 顯示文字輸入彈窗
     *
     * @param header          標題文字
     * @param confirmListener 確認按鈕點擊回調
     */
    public void showTextInputDialog(String header, OnTextInputDialogConfirmListener confirmListener) {
        showTextInputDialog(header, mActivity.getString(R.string.core_btn_confirm), confirmListener);
    }

    /**
     * 顯示文字輸入彈窗
     *
     * @param header          標題文字
     * @param confirmText     確認按鈕文字
     * @param confirmListener 確認按鈕點擊回調
     */
    public void showTextInputDialog(String header,
                                    String confirmText,
                                    final OnTextInputDialogConfirmListener confirmListener) {
        showTextInputDialog(header, confirmText, confirmListener, null);
    }

    /**
     * 顯示文字輸入彈窗
     *
     * @param header          標題文字
     * @param confirmText     確認按鈕文字
     * @param confirmListener 確認按鈕點擊回調
     * @param onKeyListener   監聽按鍵的回調
     */
    public void showTextInputDialog(String header,
                                    String confirmText,
                                    final OnTextInputDialogConfirmListener confirmListener,
                                    final DialogInterface.OnKeyListener onKeyListener) {
        // 解析布局
        View mDialogView = mInflater.inflate(R.layout.layout_dialog_text_input, null);
        // 標題
        mDialogView.<TextView>findViewById(R.id.txt_dialog_header).setText(header);

        // 輸入欄位
        EditText mEditText = mDialogView.findViewById(R.id.txt_dialog_message);
        mEditText.requestFocus();

        // 確認按鈕
        Button confirmButton = mDialogView.findViewById(R.id.btn_confirm);
        initActionButton(confirmButton, confirmText, v -> {
            if (confirmListener != null) {
                confirmListener.onDialogConfirmListener(mDialog, mEditText.getText());
            }
        });

        // 創建和顯示彈窗
        createAndShowDialog(mDialogView, false, onKeyListener);
    }

    // endregion [ 文字輸入視窗 ]

    /**
     * 顯示有圖示的彈窗
     *
     * @param icon            圖示
     * @param message         提示訊息的內容
     * @param confirmListener 確認按鈕點擊的回調
     */
    private void createHasIconDialog(int icon,
                                     String message,
                                     final OnDialogConfirmListener confirmListener) {
        createHasIconDialog(icon, message, confirmListener, null);
    }

    /**
     * 顯示有圖示的彈窗
     *
     * @param icon            圖示
     * @param message         提示訊息的內容
     * @param confirmListener 確認按鈕點擊的回調
     * @param keyListener     監聽按鍵的回調
     */
    private void createHasIconDialog(int icon,
                                     String message,
                                     final OnDialogConfirmListener confirmListener,
                                     final DialogInterface.OnKeyListener keyListener) {
        // 解析布局
        View mDialogView = mInflater.inflate(R.layout.layout_dialog_tip, null);
        // 頂部圖示
        ImageView mIconView = mDialogView.findViewById(R.id.img_dialog_icon);
        if (icon != -1) {
            mIconView.setImageResource(icon);
        } else {
            mIconView.setVisibility(View.GONE);
        }
        // 消息
        mDialogView.<TextView>findViewById(R.id.txt_dialog_message).setText(message);
        // 確認按鈕
        initActionButton(mDialogView.findViewById(R.id.btn_confirm), mActivity.getString(R.string.core_btn_confirm),
                v -> {
                    if (confirmListener != null) {
                        confirmListener.onDialogConfirmListener(mDialog);
                    }
                });
        //創建並顯示
        createAndShowDialog(mDialogView, false, keyListener);
    }

    /**
     * 創建和顯示彈窗
     *
     * @param mContentView 畫面物件
     * @param cancelable   能不能點擊空白的地方
     * @param keyListener  監聽按鍵的回調
     */
    private void createAndShowDialog(View mContentView, boolean cancelable, final DialogInterface.OnKeyListener keyListener) {
        // 先關閉之前的彈窗
        dismissDialog();
        // 創建彈窗
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity, mStyle);
        builder.setView(mContentView);
        builder.setCancelable(cancelable);
        builder.setOnCancelListener(this);
        if (keyListener != null) {
            builder.setOnKeyListener(keyListener);
        }
        mDialog = builder.create();

        try {
            mDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 初始化點擊按鈕
     *
     * @param button   需要設置的按鈕
     * @param showText 顯示的文字
     */
    private void initActionButton(Button button, String showText, final View.OnClickListener onClickListener) {
        button.setText(showText == null ? mActivity.getString(R.string.core_btn_confirm) : showText);
        button.setOnClickListener(v -> {
            dismissDialog();
            onClickListener.onClick(v);
        });
    }

    /**
     * 關閉彈窗
     */
    public void dismissDialog() {
        if (mDialog != null) {
            try {
                mDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            try {
                mProgressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        if (mDialogCancelListener != null) {
            mDialogCancelListener.onDialogCancelListener(mDialog);
        }
    }

    @Override
    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
        if (mDialogKeyListener != null) {
            mDialogKeyListener.onDialogKeyListener(mDialog, keyCode, event);
        }
        return false;
    }
}
