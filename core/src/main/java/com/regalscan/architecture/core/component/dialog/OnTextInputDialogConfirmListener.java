package com.regalscan.architecture.core.component.dialog;

import androidx.appcompat.app.AlertDialog;

public interface OnTextInputDialogConfirmListener {
    /**
     * 確認按鈕點擊的回調
     *
     * @param dialog {@link AlertDialog}
     * @param obj    obj
     */
    void onDialogConfirmListener(AlertDialog dialog, Object obj);
}
