package com.regalscan.architecture.core.net;

import okhttp3.Interceptor;

/**
 * 網路設定檔
 * <p>在 Application 中使用</p>
 */
public interface NetConfig {
    /**
     * 請求位址
     *
     * @return String
     */
    String BaseUrl();

    /**
     * 攔截器
     *
     * @return Interceptor[]
     */
    Interceptor[] Interceptors();

    /**
     * 連線逾時時間
     *
     * @return long
     */
    long ConnectTimeoutMills();

    /**
     * 讀取逾時時間
     *
     * @return long
     */
    long ReadTimeoutMills();

    /**
     * 是否調試模式
     *
     * @return boolean
     */
    boolean isLogEnable();
}
