package com.regalscan.architecture.core.net.logger;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * 紀錄呼叫 WebApi 時的位址與參數內容
 */
public class Logger {

    private final String tracePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + File.separator + "Log" + File.separator + "Trace";

    private SimpleDateFormat simpleDateFormat;
    private SimpleDateFormat simpleDateFormatDetail;

    private static final Object mLock = new Object();
    private static volatile Logger instance = null;

    public static Logger getInstance() {
        if (instance == null) {
            synchronized (mLock) {
                if (instance == null) {
                    instance = new Logger();
                }
            }
        }

        return instance;
    }

    public void init() {
        try {
            simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            simpleDateFormatDetail = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT+8:00"));
            simpleDateFormatDetail.setTimeZone(TimeZone.getTimeZone("GMT+8:00"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void writeLog(String log) {

        try {
            String date = simpleDateFormatDetail.format(new Date(System.currentTimeMillis()));
            String data = " ╠ " + date + " " + getClassName() + " " + getMethodName() + getLineNumber() + " -" + log + "\r";

            String todayDate = simpleDateFormat.format(new Date(System.currentTimeMillis()));

            writeToFile(tracePath, "L" + todayDate + ".txt", data);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void writeToFile(String filePath, String fileName, String log) {
        try {
            File path = new File(filePath);
            if (!path.exists()) {
                return;
//                path.mkdirs();

//                reScanFolder(context, path.getPath());
            }

            File file = new File(filePath, fileName);

            FileOutputStream fos = new FileOutputStream(file, true);
            BufferedOutputStream bos = new BufferedOutputStream(fos);
            bos.write(log.getBytes());
            bos.flush();
            bos.close();
            fos.flush();
            fos.close();

//            reScanFolder(context, file.getPath());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 重新掃描指定的路徑
     *
     * @param context  {@link Context}
     * @param filePath 檔案完整路徑
     */
    @SuppressWarnings("unused")
    private boolean reScanFolder(Context context, String filePath) {
        if (filePath == null) {
            return false;
        }

        File file = new File(filePath);
        // 資料夾不更新
        if (file.isDirectory()) {
            return false;
        }

        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(file);
        mediaScanIntent.setData(contentUri);
        context.sendBroadcast(mediaScanIntent);

        return true;
    }

    @SuppressWarnings("ConstantConditions")
    private String getClassName() {
        String fileName = getApplicationStackTrace().getFileName();
        if (fileName == null) {
            fileName = "     ";
        }

        return String.format("%1$-25s", fileName.substring(0, fileName.length() - 5));
    }

    @SuppressWarnings("ConstantConditions")
    private String getMethodName() {
        String methodName = getApplicationStackTrace().getMethodName();

        return String.format("%1$-21s", methodName);
    }

    @SuppressWarnings("ConstantConditions")
    private String getLineNumber() {
        int lineNumber = getApplicationStackTrace().getLineNumber();

        return String.format("%1$-4s", lineNumber);
    }

    private StackTraceElement getApplicationStackTrace() {
        boolean isLoggerClass = false;
        String className = Logger.class.getName();

        for (StackTraceElement element : Thread.currentThread().getStackTrace()) {
            if (className.equals(element.getClassName())) {
                isLoggerClass = true;
            }

            if (!className.equals(element.getClassName()) && isLoggerClass) {
                return element;
            }
        }

        return null;
    }
}
