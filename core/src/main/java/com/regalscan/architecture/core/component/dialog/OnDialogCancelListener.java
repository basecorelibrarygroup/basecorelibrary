package com.regalscan.architecture.core.component.dialog;

import androidx.appcompat.app.AlertDialog;

public interface OnDialogCancelListener {
    /**
     * 取消按鈕點擊的回調
     *
     * @param dialog {@link AlertDialog}
     */
    void onDialogCancelListener(AlertDialog dialog);
}
