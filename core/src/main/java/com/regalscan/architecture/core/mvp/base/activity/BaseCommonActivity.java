package com.regalscan.architecture.core.mvp.base.activity;

import android.graphics.Color;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;

import com.regalscan.architecture.core.R;
import com.regalscan.architecture.core.base.activity.BaseDialogActivity;
import com.regalscan.architecture.core.mvp.presenter.BasePresenter;
import com.regalscan.architecture.core.mvp.view.BaseView;
import com.regalscan.architecture.core.net.exception.NetErrorException;

public abstract class BaseCommonActivity<P extends BasePresenter<?>> extends BaseDialogActivity implements BaseView {
    private P mPresenter;

    protected Toolbar mToolbar;
    /**
     * 是否初始化了 Toolbar
     */
    protected boolean isInitToolbar = false;

    /**
     * 創建 Presenter
     *
     * @return Presenter 的實例
     */
    protected abstract P createPresenter();

    @Override
    protected void onStart() {
        super.onStart();

        if (!isInitToolbar) {
            setToolbar();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // 銷毀 取消訂閱
        if (mPresenter != null) {
//            mPresenter.unDisposable();
            mPresenter.dispose();
        }
    }

    /**
     * 底層獲取 Presenter
     *
     * @return P
     */
    protected synchronized P getPresenter() {
        if (mPresenter == null) {
            mPresenter = createPresenter();
        }
        return mPresenter;
    }

    @Override
    public void onLoading(@StringRes int resId) {
        showLoadingDialog(resId);
    }

    @Override
    public void onLoading(@StringRes int resId, boolean cancelable) {
        showLoadingDialog(resId, cancelable);
    }

    @Override
    public void onLoading(String message) {
        showLoadingDialog(message);
    }

    @Override
    public void onLoading(String message, boolean cancelable) {
        showLoadingDialog(message, cancelable);
    }

    // region [ 使用過時的 ProgressDialog ]

    @Override
    public void onLoadingOlder(int resId) {
        setProgressMessage(resId);
    }

    @Override
    public void onLoadingOlder(int resId, boolean cancelable) {
        setProgressMessage(resId, cancelable);
    }

    @Override
    public void onLoadingOlder(String message) {
        setProgressMessage(message);
    }

    @Override
    public void onLoadingOlder(String message, boolean cancelable) {
        setProgressMessage(message, cancelable);
    }

    // endregion [ 使用過時的 ProgressDialog ]

    @Override
    public void onMessage(@StringRes int resId) {
        showMessageDialog(resId);
    }

    @Override
    public void onMessage(String message) {
        showMessageDialog(message);
    }

    @Override
    public void onProgressMessage(@StringRes int resId) {
        setProgressMessage(resId);
    }

    @Override
    public void onProgressMessage(String message) {
        setProgressMessage(message);
    }

    @Override
    public void onWarning(@StringRes int resId) {
        showWarningDialog(resId);
    }

    @Override
    public void onWarning(String message) {
        showWarningDialog(message);
    }

    @Override
    public void onError(@StringRes int resId) {
        showErrorDialog(resId);
    }

    @Override
    public void onError(String message) {
        showErrorDialog(message);
    }

    @Override
    public void onError(Exception exception) {
        showErrorDialog(exception.getMessage());
    }

    @Override
    public void onError(NetErrorException exception) {
        showErrorDialog(exception.getMessage());
    }

    @Override
    public void onComplete() {
        // 請求完成，關閉 Dialog
        onDismissDialog(0);
    }

    @Override
    public void onDialogCancelListener(AlertDialog dialog) {
        super.onDialogCancelListener(dialog);
        // Dialog 取消，取消訂閱
        if (mPresenter != null) {
            mPresenter.unDisposable();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        slideLeftOut();
    }

    protected void setToolbar() {
        setToolbar(R.id.base_toolbar);
    }

    protected void setToolbar(int resId) {
        setToolbar(resId, "");
    }

    protected void setToolbar(int resId, String mainTitle) {
        setToolbar(resId, mainTitle, "");
    }

    protected void setToolbar(int resId, String mainTitle, String subTitle) {
        mToolbar = findViewById(resId);
        if (mToolbar != null) {
            // 設置主要標題
            mToolbar.setTitle(mainTitle);
            // 設置次要標題
            if (!TextUtils.isEmpty(subTitle)) {
                mToolbar.setSubtitle(subTitle);
            }
            // 設置為透明色
            mToolbar.setBackgroundColor(Color.TRANSPARENT);
            // 設置為全透明
            mToolbar.getBackground().setAlpha(0);
            setSupportActionBar(mToolbar);

            //子類中沒有設置過返回按鈕的情況下
            if (mToolbar.getNavigationIcon() == null) {
                // 設置返回按鈕
                mToolbar.setNavigationIcon(R.drawable.arrow_back_24);
            }

//            mToolbar.setNavigationOnClickListener(view -> {
//                // 返回按鈕點擊
//                finish();
//                slideLeftOut();
//            });

            // 返回文字按鈕
            View navText = findViewById(R.id.toolbar_nav_text);
            if (navText != null) {
                navText.setOnClickListener(view -> {
                    // 返回按鈕點擊
                    finish();
                    slideLeftOut();
                });
            }

            isInitToolbar = true;
        }
    }
}
