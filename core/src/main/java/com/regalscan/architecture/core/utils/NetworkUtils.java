package com.regalscan.architecture.core.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * 網路狀態工具
 * 參考 https://developer.android.com/training/basics/network-ops/managing?hl=zh-tw
 */
public class NetworkUtils {

    /**
     * 確認網路是否已連線
     *
     * @param context {@link Context}
     * @return 是否已連線
     */
    public static boolean isOnline(Context context) {
        if (context == null) {
            return false;
        }

        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        return networkInfo != null && networkInfo.isConnected();
    }
}
