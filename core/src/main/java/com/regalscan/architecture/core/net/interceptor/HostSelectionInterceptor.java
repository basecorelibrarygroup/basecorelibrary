package com.regalscan.architecture.core.net.interceptor;

import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.regalscan.architecture.core.constant.BaseObj;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class HostSelectionInterceptor implements Interceptor {

    @NonNull
    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        // 獲取原始的請求
        Request mRequest = chain.request();
        // 獲取舊 URL
        HttpUrl mHttpUrl = mRequest.url();
        // 獲取 request 的 Builder
        Request.Builder mBuilder = mRequest.newBuilder();

        // 通過給定的 key url_name, 從 Request 獲取 Header 的集合, 如 default、update
        List<String> mUrlNameList = mRequest.headers(BaseObj.DOMAIN_NAME);
        if (mUrlNameList.size() > 0) {
            // 刪除原有配置中的值
            mBuilder.removeHeader(BaseObj.DOMAIN_NAME);
            // 獲取 Header 中配置的值, 如 default、update
            String mUrlName = mUrlNameList.get(0);
            HttpUrl mBaseUrl = null;
            // 根據 Header 中配置的 value，來匹配新的 URL 地址
            switch (mUrlName) {
                case BaseObj.DOMAIN_NAME_WEBAPI:
                    mBaseUrl = HttpUrl.parse(BaseObj.URL_WEBAPI);
                    break;
                case BaseObj.DOMAIN_NAME_UPDATE:
                    mBaseUrl = HttpUrl.parse(BaseObj.URL_UPDATE);
                    break;
            }

            // 重建新的 HttpUrl，需要重新設置 url 的部分
            HttpUrl.Builder newBuilder = new HttpUrl.Builder()
                    .scheme(mBaseUrl.scheme())   // 協定 ( http or https )
                    .host(mBaseUrl.host())       // 更換主機名稱
                    .port(mBaseUrl.port());       // 更換通訊埠

            // region [ 添加 Path ]
            List<String> segments = new ArrayList<>();
            segments.addAll(mBaseUrl.pathSegments());
            segments.addAll(mHttpUrl.pathSegments());

            // region [ 移除重複的 segment ]
            List<String> mTemp = new ArrayList<>();
            for (String str : segments) {
                if (!mTemp.contains(str)) {
                    mTemp.add(str);
                }
            }

            segments.clear();
            segments.addAll(mTemp);
            // endregion [ 移除重複的 segment ]

            for (String segment : segments) {
                if (!TextUtils.isEmpty(segment)) {
                    newBuilder.addPathSegment(segment);
                }
            }
            // endregion [ 添加 Path ]

            mHttpUrl = newBuilder.build();

            // 重建這個 Request
            HttpUrl.Builder builder = mHttpUrl.newBuilder();
            // 添加參數
            builder.addQueryParameter("name", BaseObj.OPERATION_NAME);
//            builder.addQueryParameter("version", BuildConfig.VERSION_NAME);
            builder.addQueryParameter("version", BaseObj.VERSION_NAME);

            mRequest = mRequest.newBuilder()
                    .url(builder.build())
                    .build();
        }

        // region [ 舊方式 ]
//        // 拿到請求
//        Request request = chain.request();
//
//        HttpUrl httpUrl = request.url();
//        HttpUrl.Builder newUrlBuilder = httpUrl.newBuilder();
//
//        // 替換 Host
//        String host = httpUrl.host();
//        // 取得舊的 web api url 的 host name
//        String web_api_host = (new URL(ShareObj.URL_WEBAPI)).getHost();
//        // 這裡是判斷，當然真實情況不會那麼簡單
//        if (!host.equals(web_api_host)) {
//            host = web_api_host;
//        }
//
//        // 重新設置新的 Host
//        newUrlBuilder.host(host);
//
//        // region [ 替換 Path ]
//        // 真實項目中，判斷條件更加複雜。
//        // region [ 舉例如下 ]
//        // http://192.168.6.146/RegalWebApi/api/Basic/Test
//        // 第一個 segment 為 RegalWebApi
//        // 第二個 segment 為 api
//        // 第三個 segment 為 Basic
//        // 第四個 segment 為 Test
//        // end region [ 舉例如下 ]
//        // List<String> pathSegments = httpUrl.pathSegments();
//        // 將 index 的 segment 替換為傳入的值
//        // newUrlBuilder.setPathSegment(index, segment);
//
//        // 這裡是我已經知道了我是要移除第一個路徑，所以我直接移除了
//        // newUrlBuilder.removePathSegment(0);
//        // end region [ 替換 Path ]
//
//        // 添加參數
////        newUrlBuilder.addQueryParameter("version", "v1.0.0");
//        newUrlBuilder.addQueryParameter("version", BuildConfig.VERSION_NAME);
//
//        // 創建新的請求
//        request = request.newBuilder()
//                .url(newUrlBuilder.build())
////                .header("NewHeader", "NewHeaderValue")
//                .build();
        // endregion [ 舊方式 ]

        return chain.proceed(mRequest);
    }
}
