package com.regalscan.architecture.core.mvp.view;

import androidx.annotation.StringRes;

import com.regalscan.architecture.core.net.exception.NetErrorException;

/**
 * 基類 View，對統一的接口進行定義
 */
public interface BaseView {
    /**
     * Loading 訊息
     *
     * @param resId 訊息
     */
    void onLoading(@StringRes int resId);

    /**
     * Loading 訊息
     *
     * @param resId      訊息
     * @param cancelable 能不能點擊空白的地方來關閉視窗
     */
    void onLoading(@StringRes int resId, boolean cancelable);

    /**
     * Loading 訊息
     *
     * @param message 訊息
     */
    void onLoading(String message);

    /**
     * Loading 訊息
     *
     * @param message    訊息
     * @param cancelable 能不能點擊空白的地方來關閉視窗
     */
    void onLoading(String message, boolean cancelable);

    // region [ 使用過時的 ProgressDialog ]

    /**
     * Loading 訊息
     *
     * @param resId 訊息
     */
    void onLoadingOlder(@StringRes int resId);

    /**
     * Loading 訊息
     *
     * @param resId      訊息
     * @param cancelable 能不能點擊空白的地方來關閉視窗
     */
    void onLoadingOlder(@StringRes int resId, boolean cancelable);

    /**
     * Loading 訊息
     *
     * @param message 訊息
     */
    void onLoadingOlder(String message);

    /**
     * Loading 訊息
     *
     * @param message    訊息
     * @param cancelable 能不能點擊空白的地方來關閉視窗
     */
    void onLoadingOlder(String message, boolean cancelable);
    // endregion [ 使用過時的 ProgressDialog ]

    /**
     * 訊息
     *
     * @param resId 訊息
     */
    void onMessage(@StringRes int resId);

    /**
     * 訊息
     *
     * @param message 訊息
     */
    void onMessage(String message);

    /**
     * 訊息
     *
     * @param resId 訊息
     */
    void onProgressMessage(@StringRes int resId);

    /**
     * 訊息
     *
     * @param message 訊息
     */
    void onProgressMessage(String message);

    /**
     * 警告訊息
     *
     * @param resId 警告訊息
     */
    void onWarning(@StringRes int resId);

    /**
     * 警告訊息
     *
     * @param message 警告訊息
     */
    void onWarning(String message);

    /**
     * 回調失敗訊息
     *
     * @param resId 錯誤訊息
     */
    void onError(@StringRes int resId);

    /**
     * 回調失敗訊息
     *
     * @param message 錯誤訊息
     */
    void onError(String message);

    /**
     * 回調失敗訊息
     *
     * @param exception exception 異常內容
     */
    void onError(Exception exception);

    /**
     * 回調失敗訊息
     *
     * @param exception exception 異常內容
     */
    void onError(NetErrorException exception);

    /**
     * 完成網路請求，可以在這個方法中關閉彈出操作
     */
    void onComplete();
}
