package com.regalscan.architecture.core.component.dialog;

import android.view.KeyEvent;

import androidx.appcompat.app.AlertDialog;

public interface OnDialogKeyListener {
    /**
     * onKey 的回調
     *
     * @param dialog dialog 彈窗
     */
    void onDialogKeyListener(AlertDialog dialog, int i, KeyEvent keyEvent);
}
