package com.regalscan.architecture.core.mvp.model;

import android.content.ContentResolver;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.regalscan.architecture.core.constant.BaseObj;
import com.regalscan.architecture.core.mvp.presenter.BasePresenter;

import io.reactivex.rxjava3.disposables.Disposable;
import okhttp3.RequestBody;

/**
 * 基類 Model，做統一的處理
 */
public abstract class BaseModel<P extends BasePresenter<?>> {
    protected P presenter;
    @NonNull
    protected ContentResolver resolver;
    /**
     * 持有訂閱
     */
    public Disposable mDisposable;

    protected Gson gson = new Gson();

    public BaseModel(@NonNull P presenter, @NonNull ContentResolver resolver) {
        this.presenter = presenter;
        this.resolver = resolver;
    }

    /**
     * 取消訂閱
     */
    public void unDisposable() {
        if (mDisposable != null) {
            mDisposable.dispose();
        }
    }

    /**
     * 使用指定的 class 類別, 反序列化傳入的物件
     *
     * @param obj 需反序列化的物件
     * @param <T> 指定的 class 類別
     * @return 返回完成反序列化的指定類別物件
     */
    protected <T> T JsonDeserialize(Object obj) {
        JsonElement element = gson.toJsonTree(obj);
        return gson.fromJson(element, new TypeToken<T>() {
        }.getType());
    }

    /**
     * 物件轉換成 Json 序列化物件
     *
     * @param obj 需序列化的物件
     * @return 返回序列化後的字串
     */
    protected String JsonSerialize(Object obj) {
        return gson.toJson(obj);
    }

    /**
     * 將 Json 字串轉換成網路請求體 {@link RequestBody} 物件
     *
     * @param json 要轉換的字串
     * @return 網路請求體 {@link RequestBody} 物件
     */
    protected RequestBody createRequestBody(String json) {
        return RequestBody.create(json, BaseObj.MEDIA_TYPE_JSON);
    }

    /**
     * 將物件轉換成網路請求體 {@link RequestBody} 物件
     *
     * @param obj 要轉換的物件
     * @return 網路請求體 {@link RequestBody} 物件
     */
    protected RequestBody createRequestBody(Object obj) {
        return RequestBody.create(gson.toJson(obj), BaseObj.MEDIA_TYPE_JSON);
    }
}
