package com.regalscan.architecture.core.net;

import com.google.gson.JsonParseException;
import com.regalscan.architecture.core.mvp.view.BaseView;
import com.regalscan.architecture.core.net.exception.NetErrorException;

import org.json.JSONException;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import io.reactivex.rxjava3.subscribers.ResourceSubscriber;
import retrofit2.HttpException;

/**
 * 統一訂閱管理
 *
 * @param <T> 泛型
 */
public abstract class ApiSubscriber<T> extends ResourceSubscriber<T> {
    private BaseView mView;

    public ApiSubscriber() {
    }

    public ApiSubscriber(BaseView view) {
        mView = view;
    }

    @Override
    public void onNext(T t) {

    }

    @Override
    public void onError(Throwable t) {
        NetErrorException error = null;
        if (t != null) {
            // 對不是自訂義拋出的錯誤進行解析
            if (!(t instanceof NetErrorException)) {
                if (t instanceof UnknownHostException) {
                    error = new NetErrorException(t, NetErrorException.NoConnectError);
                } else if (t instanceof JSONException || t instanceof JsonParseException) {
                    error = new NetErrorException(t, NetErrorException.ParserError);
                } else if (t instanceof SocketTimeoutException) {
                    error = new NetErrorException(t, NetErrorException.SocketTimeoutError);
                } else if (t instanceof ConnectException) {
                    error = new NetErrorException(t, NetErrorException.ConnectError);
                } else if (t instanceof HttpException) {
                    error = new NetErrorException(t, NetErrorException.HttpError);
                } else {
                    error = new NetErrorException(t, NetErrorException.OTHER);
                }
            } else {
                error = new NetErrorException(t.getMessage(), NetErrorException.OTHER);
            }
        }

        // 回調抽象方法
        onFail(error);
    }

    /**
     * 回調錯誤
     *
     * @param error {@link NetErrorException}
     */
    protected void onFail(NetErrorException error) {
        if (mView != null) {
            // 這邊覆寫 onComplete 會導致錯誤視窗顯示後, 馬上關閉, 所以註解掉
//            mView.onComplete();
            mView.onError(error);
        }
    }

    @Override
    public void onComplete() {
        // 這邊覆寫 onComplete 會導致其他視窗顯示後, 馬上關閉, 所以註解掉
//        if (mView != null) {
//            mView.onComplete();
//        }
    }
}
