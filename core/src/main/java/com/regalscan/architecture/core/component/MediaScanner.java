package com.regalscan.architecture.core.component;

import android.content.Context;
import android.media.MediaScannerConnection;
import android.net.Uri;

import java.io.File;

public class MediaScanner implements MediaScannerConnection.MediaScannerConnectionClient {
    private final String TAG = MediaScanner.class.getSimpleName();
    private final MediaScannerConnection mScanner;
    private final ScanListener mListener;
    private final File mFile;
    private final String mMimeType;

    public MediaScanner(Context context, File file, String mimeType, ScanListener listener) {
        mFile = file;
        mListener = listener;
        mMimeType = mimeType;

        mScanner = new MediaScannerConnection(context, this);
        mScanner.connect();
    }

    @Override
    public void onMediaScannerConnected() {
        if (mScanner == null)
            return;

        scan(mFile, mMimeType);
    }

    @Override
    public void onScanCompleted(String path, Uri uri) {
        if (mScanner == null)
            return;

        mScanner.disconnect();
        mListener.onScanFinish();
    }

    private void scan(File file, String mimeType) {
        if (file.isFile()) {
            mScanner.scanFile(file.getAbsolutePath(), mimeType);
            return;
        }

        File[] files = file.listFiles();
        if (files == null)
            return;

        for (File f : files) {
            scan(f, mimeType);
        }
    }

    public interface ScanListener {
        void onScanFinish();
    }
}
