package com.regalscan.architecture.core.utils;

public enum NumericType {
    TypeShort,
    TypeInteger,
    TypeLong,
    TypeFloat,
    TypeDouble,
}
