package com.regalscan.architecture.core.net.interceptor;

import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.regalscan.architecture.core.constant.BaseObj;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Headers;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class OAuthInterceptor implements Interceptor {
    @NonNull
    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {

        Request oldRequest = chain.request();
        Request.Builder builder = oldRequest.newBuilder();

        if (!TextUtils.isEmpty(BaseObj.TOKEN)) {
            builder.headers(Headers.of(getHeaders()));
        }

        Request request = builder.build();

//        Request request = oldRequest.newBuilder()
//                .headers(Headers.of(getHeaders()))
//                .build();

        return chain.proceed(request);
    }

    private Map<String, String> getHeaders() {
        HashMap<String, String> map = new HashMap<>();
        map.put(BaseObj.AUTHORIZATION, BaseObj.AUTHORIZATION_BEARER + BaseObj.TOKEN);
        return map;
    }
}
