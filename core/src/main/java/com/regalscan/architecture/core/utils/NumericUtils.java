package com.regalscan.architecture.core.utils;

import androidx.annotation.NonNull;

import java.text.DecimalFormat;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NumericUtils {

    /**
     * 字串補零
     *
     * @param numericType 指定要轉換的型態
     *                    {@link NumericType}
     * @param digits      位數
     *                    範例
     *                    整數帶千分位 {@code String digits = "0,000"; or %,04d; }
     *                    浮點數帶千分位 {@code String digits = "0,000.000"; or %,.3f; }
     * @param obj         傳入的值
     * @return 完成補零的字串
     * @throws Exception e
     * @see NumericType
     */
    public static String fixZero(@NonNull NumericType numericType, @NonNull String digits, @NonNull Object obj) throws Exception {
        try {
            if (!isNumeric(obj)) {
                throw new Exception("不為數字");
            }

            String numeric = obj.toString().trim();
            Object o = null;

            // region [ 將傳入的字串轉為相對應的數字型態 ]
            switch (numericType) {
                case TypeShort:
                    o = Short.parseShort(numeric);
                    break;
                case TypeInteger:
                    o = Integer.parseInt(numeric);
                    break;
                case TypeLong:
                    o = Long.parseLong(numeric);
                    break;
                case TypeFloat:
                    o = Float.parseFloat(numeric);
                    break;
                case TypeDouble:
                    o = Double.parseDouble(numeric);
                    break;
            }

            if (o == null) {
                throw new Exception("型態轉換失敗");
            }
            // endregion [ 將傳入的字串轉為相對應的數字型態 ]

            if (digits.startsWith("%")) {
                return String.format(Locale.getDefault(), digits, o);
            } else {
                DecimalFormat decimalFormat = new DecimalFormat(digits);
                return decimalFormat.format(o);
            }
        } catch (Exception e) {
            throw new Exception(e);
        }
    }

    /**
     * 判斷是否為數字
     *
     * @param obj 需要判斷的值
     * @return 是否為數字
     * @throws Exception Exception e
     */
    public static boolean isNumeric(@NonNull Object obj) throws Exception {
        try {
            String numeric = String.valueOf(obj);

            Pattern pattern = Pattern.compile("^[0-9.]+$");
            Matcher matcher = pattern.matcher(numeric);
            return matcher.matches();
        } catch (Exception e) {
            throw new Exception(e);
        }
    }
}
