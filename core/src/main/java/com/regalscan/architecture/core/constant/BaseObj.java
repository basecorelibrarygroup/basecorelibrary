package com.regalscan.architecture.core.constant;

import okhttp3.MediaType;

public class BaseObj {

    // region [ 共用變數 ]
    public static String OPERATION_NAME = "";
    public static String VERSION_NAME = "";
    public static int VERSION_CODE = 0;

    // region [ 網路專用 ]
    public final static String DOMAIN_NAME = "Domain-Name";
    public final static String DOMAIN_NAME_HEADER = DOMAIN_NAME + ":";
    public final static String DOMAIN_NAME_WEBAPI = "webapi";
    public final static String DOMAIN_NAME_UPDATE = "update";
    public final static String AUTHORIZATION = "Authorization";
    public final static String AUTHORIZATION_BASIC = "Basic";
    public final static String AUTHORIZATION_BEARER = "Bearer";
    public static String URL_WEBAPI = "";
    public static String URL_UPDATE = "";
    public static String TOKEN = "";
    public static MediaType MEDIA_TYPE_JSON = MediaType.parse("application/json; charset=utf-8");
    // endregion [ 網路專用 ]

    // endregion [ 共用變數 ]
}
