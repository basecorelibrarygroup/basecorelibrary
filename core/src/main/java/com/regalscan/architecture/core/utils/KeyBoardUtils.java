package com.regalscan.architecture.core.utils;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

public class KeyBoardUtils {

    public static void hideKeyBoard(@NonNull View view) {
        KeyBoardOperation(view, 2);
    }

    public static void hideKeyBoard(@NonNull Activity activity) {
        View view = activity.getCurrentFocus();
        if (view != null) {
            KeyBoardOperation(view, 2);
        }
    }

    public static void hideKeyBoard(@NonNull Fragment fragment) {
        View view = fragment.getView().getRootView();
        if (view != null) {
            KeyBoardOperation(view, 2);
        }
    }

    public static void showKeyBoard(@NonNull View view) {
        KeyBoardOperation(view, 1);
    }

    public static void toggleKeyBoard(@NonNull View view) {
        KeyBoardOperation(view, 0);
    }

    private static void KeyBoardOperation(@NonNull View view, int type) {
        try {
            new Handler().postDelayed(() -> {
                InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    switch (type) {
                        case 0:
                            imm.toggleSoftInput(0, 0);
                            break;
                        case 1:
                            view.requestFocus();
                            imm.showSoftInput(view, InputMethodManager.SHOW_FORCED);
                            break;
                        case 2:
                            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                            break;
                    }
                }
            }, 100);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }
}
