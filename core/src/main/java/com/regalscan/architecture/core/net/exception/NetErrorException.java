package com.regalscan.architecture.core.net.exception;

import android.text.TextUtils;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.HttpException;
import retrofit2.Response;

public class NetErrorException extends IOException {
    /**
     * 無連接異常
     */
    public static final int NoConnectError = 0;
    /**
     * 數據解析異常
     */
    public static final int ParserError = 1;
    /**
     * 無連接異常
     */
//    public static final int NO_CONNECT_ERROR = 1;
    /**
     * 網路連接超時
     */
    public static final int SocketTimeoutError = 6;
    /**
     * 無法連接到服務
     */
    public static final int ConnectError = 7;
    /**
     * 伺服器錯誤
     */
    public static final int HttpError = 8;
    /**
     * 登入失敗
     */
    public static final int LOGIN_OUT = 401;
    /**
     * 找不到
     */
    public static final int NOT_FOUND = 404;
    /**
     * 沒有網路
     */
    public static final int UNOKE = -1;
    /**
     * 其他
     */
    public static final int OTHER = -99;

    private Throwable exception;
    private int mErrorType = NoConnectError;
    private String mErrorMessage;
    private Gson gson = new Gson();

    public NetErrorException(Throwable exception, int mErrorType) {
        this.exception = exception;
        this.mErrorType = mErrorType;
//        this.mErrorMessage = TextUtils.isEmpty(exception.getMessage()) ? "" : exception.getMessage();
    }

    public NetErrorException(String message, Throwable cause) {
        super(message, cause);
    }

    public NetErrorException(String message, int mErrorType) {
        super(message);
        this.mErrorType = mErrorType;
        this.mErrorMessage = message;
    }

    @Override
    public String getMessage() {
        if (!TextUtils.isEmpty(mErrorMessage)) {
            return mErrorMessage;
        }

        try {
            switch (mErrorType) {
                case NoConnectError:
                    return "無連接異常";
                case ParserError:
                    return "數據解析異常";
                case SocketTimeoutError:
                    return "連線逾時";
                case ConnectError:
                    return "無法連接到伺服器，請檢查網路連接後再試 !";
                case HttpError:
                    Response<?> response = ((HttpException) exception).response();
                    ResponseBody respBody = null;
                    if (response != null) {
                        respBody = response.errorBody();
                    }
                    String errorBody = "";
                    try {
                        errorBody = respBody == null ? "" : new JSONObject(respBody.string()).getString("Message");
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }

                    if (TextUtils.isEmpty(errorBody)) {
                        errorBody = ((HttpException) exception).getMessage();
                    }

                    switch (((HttpException) exception).code()) {
                        case 400:
                        case 404:
                            return errorBody;
//                            return "無法連接到伺服器，請檢察網路連接後再試!";
                        case 500:
                            return "伺服器發生錯誤!";
                        case 503:
                            return "找不到服務!";
                        default:
                            return String.format("狀態碼: %s\n狀態訊息: %s", ((HttpException) exception).code(), ((HttpException) exception).message());
                    }
                case UNOKE:
                    return "當前無網路連接";
                case OTHER:
                    try {
                        if (TextUtils.isEmpty(mErrorMessage)) {
                            mErrorMessage = exception.getMessage();
                        }
                        if (TextUtils.isEmpty(mErrorMessage)) {
                            mErrorMessage = "未知錯誤";
                        }
                        return mErrorMessage;
                    } catch (Exception e) {
                        return "未知錯誤\n" + e.toString();
                    }
//                    return !TextUtils.isEmpty(mErrorMessage) ? mErrorMessage : "未知錯誤";
            }

            return exception.getMessage();
        } catch (Exception e) {
            return "未知錯誤";
        }
    }

    /**
     * 獲取錯誤類型
     *
     * @return 錯誤編號
     */
    public int getErrorType() {
        return mErrorType;
    }
}