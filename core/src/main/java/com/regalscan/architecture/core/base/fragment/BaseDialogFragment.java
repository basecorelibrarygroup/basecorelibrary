package com.regalscan.architecture.core.base.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;

import com.regalscan.architecture.core.component.dialog.DialogHelper;
import com.regalscan.architecture.core.component.dialog.OnDialogCancelListener;
import com.regalscan.architecture.core.component.dialog.OnDialogConfirmListener;
import com.regalscan.architecture.core.component.dialog.OnDialogKeyListener;
import com.regalscan.architecture.core.component.dialog.OnPasswordVerifyDialogConfirmListener;
import com.regalscan.architecture.core.component.dialog.OnTextInputDialogConfirmListener;

public class BaseDialogFragment extends BaseFragment implements OnDialogCancelListener, OnDialogKeyListener {
    protected DialogHelper mDialogHelper;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (mDialogHelper == null) {
            mDialogHelper = new DialogHelper(mFragment, this, this);
        }
    }

    // region [ Loading 視窗 ]

    /**
     * 顯示 Loading 彈窗
     *
     * @param resId 訊息提示
     */
    public void showLoadingDialog(@StringRes int resId) {
        mDialogHelper.showLoadingDialog(resId);
    }

    /**
     * 顯示 Loading 彈窗
     *
     * @param resId      訊息提示
     * @param cancelable 能不能點擊空白的地方
     */
    public void showLoadingDialog(@StringRes int resId, boolean cancelable) {
        mDialogHelper.showLoadingDialog(resId, cancelable);
    }

    /**
     * 顯示 Loading 彈窗，默認不能點擊空白處進行取消
     *
     * @param loadingTip 訊息提示
     */
    public void showLoadingDialog(String loadingTip) {
        mDialogHelper.showLoadingDialog(loadingTip);
    }

    /**
     * 顯示 Loading 彈窗
     *
     * @param loadingTip 訊息提示
     * @param cancelable 能不能點擊空白的地方
     */
    public void showLoadingDialog(String loadingTip, Boolean cancelable) {
        mDialogHelper.showLoadingDialog(loadingTip, cancelable);
    }
    // endregion [ Loading 視窗 ]
    // region [ Message 視窗 ]

    /**
     * 訊息提示彈窗
     *
     * @param resId 提示訊息的內容
     */
    public void showMessageDialog(@StringRes int resId) {
        mDialogHelper.showMessageDialog(resId);
    }

    /**
     * 訊息提示彈窗
     *
     * @param resId           提示訊息的內容
     * @param confirmListener 確認按鈕點擊的回調
     */
    public void showMessageDialog(@StringRes int resId, OnDialogConfirmListener confirmListener) {
        mDialogHelper.showMessageDialog(resId, confirmListener);
    }

    /**
     * 訊息提示彈窗
     *
     * @param resId           提示訊息的內容
     * @param confirmListener 確認按鈕點擊的回調
     * @param keyListener     監聽按鍵的回調
     */
    public void showMessageDialog(@StringRes int resId, OnDialogConfirmListener confirmListener, DialogInterface.OnKeyListener keyListener) {
        mDialogHelper.showMessageDialog(resId, confirmListener, keyListener);
    }

    /**
     * 訊息提示彈窗
     *
     * @param message 提示訊息的內容
     */
    public void showMessageDialog(String message) {
        mDialogHelper.showMessageDialog(message);
    }

    /**
     * 訊息提示彈窗
     *
     * @param message         提示訊息的內容
     * @param confirmListener 確認按鈕點擊的回調
     */
    public void showMessageDialog(String message, OnDialogConfirmListener confirmListener) {
        mDialogHelper.showMessageDialog(message, confirmListener);
    }

    /**
     * 訊息提示彈窗
     *
     * @param message         提示訊息的內容
     * @param confirmListener 確認按鈕點擊的回調
     * @param keyListener     監聽按鍵的回調
     */
    public void showMessageDialog(String message, OnDialogConfirmListener confirmListener, DialogInterface.OnKeyListener keyListener) {
        mDialogHelper.showMessageDialog(message, confirmListener, keyListener);
    }

    /**
     * 訊息提示彈窗
     *
     * @param resId 傳入 Resource Id 型別的字串
     */
    public void setProgressMessage(@StringRes int resId) {
        mDialogHelper.setMessage(resId);
    }

    /**
     * 訊息提示彈窗
     *
     * @param resId 傳入 Resource Id 型別的字串
     */
    public void setProgressMessage(@StringRes int resId, boolean cancelable) {
        mDialogHelper.setMessage(resId, cancelable);
    }

    /**
     * 訊息提示彈窗
     *
     * @param msg 傳入字串
     */
    public void setProgressMessage(String msg) {
        mDialogHelper.setMessage(msg);
    }

    /**
     * 訊息提示彈窗
     *
     * @param msg 傳入字串
     */
    public void setProgressMessage(String msg, boolean cancelable) {
        mDialogHelper.setMessage(msg, cancelable);
    }
    // endregion [ Message 視窗 ]
    // region [ Success 視窗 ]

    /**
     * 成功提示彈窗
     *
     * @param resId 提示訊息的內容
     */
    public void showSuccessDialog(@StringRes int resId) {
        mDialogHelper.showSuccessDialog(resId);
    }

    /**
     * 成功提示彈窗
     *
     * @param resId           提示訊息的內容
     * @param confirmListener 確認按鈕點擊的回調
     */
    public void showSuccessDialog(@StringRes int resId, OnDialogConfirmListener confirmListener) {
        mDialogHelper.showSuccessDialog(resId, confirmListener);
    }

    /**
     * 成功提示彈窗
     *
     * @param resId           提示訊息的內容
     * @param confirmListener 確認按鈕點擊的回調
     * @param keyListener     監聽按鍵的回調
     */
    public void showSuccessDialog(@StringRes int resId, OnDialogConfirmListener confirmListener, DialogInterface.OnKeyListener keyListener) {
        mDialogHelper.showSuccessDialog(resId, confirmListener, keyListener);
    }

    /**
     * 成功提示彈窗
     *
     * @param message 提示訊息的內容
     */
    public void showSuccessDialog(String message) {
        mDialogHelper.showSuccessDialog(message);
    }

    /**
     * 成功提示彈窗
     *
     * @param message         提示訊息的內容
     * @param confirmListener 確認按鈕點擊的回調
     */
    public void showSuccessDialog(String message, OnDialogConfirmListener confirmListener) {
        mDialogHelper.showSuccessDialog(message, confirmListener);
    }

    /**
     * 成功提示彈窗
     *
     * @param message         提示訊息的內容
     * @param confirmListener 確認按鈕點擊的回調
     * @param keyListener     監聽按鍵的回調
     */
    public void showSuccessDialog(String message, OnDialogConfirmListener confirmListener, DialogInterface.OnKeyListener keyListener) {
        mDialogHelper.showSuccessDialog(message, confirmListener, keyListener);
    }
    // endregion [ Success 視窗 ]
    // region [ Warning 視窗 ]

    /**
     * 警告提示彈窗
     *
     * @param resId 提示訊息的內容
     */
    public void showWarningDialog(@StringRes int resId) {
        mDialogHelper.showWarningDialog(resId);
    }

    /**
     * 警告提示彈窗
     *
     * @param resId           提示訊息的內容
     * @param confirmListener 確認按鈕點擊的回調
     */
    public void showWarningDialog(@StringRes int resId, OnDialogConfirmListener confirmListener) {
        mDialogHelper.showWarningDialog(resId, confirmListener);
    }

    /**
     * 警告提示彈窗
     *
     * @param resId           提示訊息的內容
     * @param confirmListener 確認按鈕點擊的回調
     * @param keyListener     監聽按鍵的回調
     */
    public void showWarningDialog(@StringRes int resId, OnDialogConfirmListener confirmListener, DialogInterface.OnKeyListener keyListener) {
        mDialogHelper.showWarningDialog(resId, confirmListener, keyListener);
    }

    /**
     * 警告提示彈窗
     *
     * @param message 提示訊息的內容
     */
    public void showWarningDialog(String message) {
        mDialogHelper.showWarningDialog(message);
    }

    /**
     * 警告提示彈窗
     *
     * @param message         提示訊息的內容
     * @param confirmListener 確認按鈕的回調
     */
    public void showWarningDialog(String message, OnDialogConfirmListener confirmListener) {
        mDialogHelper.showWarningDialog(message, confirmListener);
    }

    /**
     * 警告提示彈窗
     *
     * @param message         提示訊息的內容
     * @param confirmListener 確認按鈕點擊的回調
     * @param keyListener     監聽按鍵的回調
     */
    public void showWarningDialog(String message, OnDialogConfirmListener confirmListener, DialogInterface.OnKeyListener keyListener) {
        mDialogHelper.showWarningDialog(message, confirmListener, keyListener);
    }
    // endregion [ Warning 視窗 ]
    // region [ Error 視窗 ]

    /**
     * 錯誤提示彈窗
     *
     * @param resId 提示訊息的內容
     */
    public void showErrorDialog(@StringRes int resId) {
        mDialogHelper.showErrorDialog(resId);
    }

    /**
     * 錯誤提示彈窗
     *
     * @param resId           提示訊息的內容
     * @param confirmListener 確認按鈕點擊的回調
     */
    public void showErrorDialog(@StringRes int resId, OnDialogConfirmListener confirmListener) {
        mDialogHelper.showErrorDialog(resId, confirmListener);
    }

    /**
     * 錯誤提示彈窗
     *
     * @param resId           提示訊息的內容
     * @param confirmListener 確認按鈕點擊的回調
     * @param keyListener     監聽按鍵的回調
     */
    public void showErrorDialog(@StringRes int resId, OnDialogConfirmListener confirmListener, DialogInterface.OnKeyListener keyListener) {
        mDialogHelper.showErrorDialog(resId, confirmListener, keyListener);
    }

    /**
     * 錯誤提示彈窗
     *
     * @param message 提示訊息的內容
     */
    public void showErrorDialog(String message) {
        mDialogHelper.showErrorDialog(message);
    }

    /**
     * 錯誤提示彈窗
     *
     * @param message         提示訊息的內容
     * @param confirmListener 確認按鈕點擊的回調
     */
    public void showErrorDialog(String message, OnDialogConfirmListener confirmListener) {
        mDialogHelper.showErrorDialog(message, confirmListener);
    }

    /**
     * 錯誤提示彈窗
     *
     * @param message         提示訊息的內容
     * @param confirmListener 確認按鈕點擊的回調
     * @param keyListener     監聽按鍵的回調
     */
    public void showErrorDialog(String message, OnDialogConfirmListener confirmListener, DialogInterface.OnKeyListener keyListener) {
        mDialogHelper.showErrorDialog(message, confirmListener, keyListener);
    }
    // endregion [ Error 視窗 ]
    // region [ Confirm 視窗 ]

    /**
     * 顯示確認彈窗
     *
     * @param messageId       提示訊息
     * @param confirmListener 確認按鈕點擊回調
     */
    public void showConfirmDialog(@StringRes int messageId, OnDialogConfirmListener confirmListener) {
        mDialogHelper.showConfirmDialog(messageId, confirmListener);
    }

    /**
     * 顯示確認彈窗
     *
     * @param messageId       提示訊息
     * @param confirmId       確認按鈕文字
     * @param cancelId        取消按鈕文字
     * @param confirmListener 確認按鈕點擊回調
     */
    public void showConfirmDialog(@StringRes int messageId,
                                  @StringRes int confirmId,
                                  @StringRes int cancelId,
                                  OnDialogConfirmListener confirmListener) {
        mDialogHelper.showConfirmDialog(messageId, confirmId, cancelId, confirmListener);
    }

    /**
     * 顯示確認彈窗
     *
     * @param messageId       提示訊息
     * @param confirmId       確認按鈕文字
     * @param cancelId        取消按鈕文字
     * @param confirmListener 確認按鈕點擊回調
     * @param cancelListener  確認按鈕點擊的回調
     */
    public void showConfirmDialog(@StringRes int messageId,
                                  @StringRes int confirmId,
                                  @StringRes int cancelId,
                                  final OnDialogConfirmListener confirmListener,
                                  final OnDialogCancelListener cancelListener) {
        mDialogHelper.showConfirmDialog(messageId, confirmId, cancelId, confirmListener, cancelListener);
    }

    /**
     * 顯示確認彈窗
     *
     * @param messageId       提示訊息
     * @param confirmId       確認按鈕文字
     * @param cancelId        取消按鈕文字
     * @param confirmListener 確認按鈕點擊回調
     * @param cancelListener  確認按鈕點擊的回調
     * @param onKeyListener   取消按鈕點擊的回調
     */
    public void showConfirmDialog(@StringRes int messageId,
                                  @StringRes int confirmId,
                                  @StringRes int cancelId,
                                  final OnDialogConfirmListener confirmListener,
                                  final OnDialogCancelListener cancelListener,
                                  final DialogInterface.OnKeyListener onKeyListener) {
        mDialogHelper.showConfirmDialog(messageId, confirmId, cancelId, confirmListener, cancelListener, onKeyListener);
    }

    /**
     * 顯示確認彈窗
     *
     * @param message         提示訊息
     * @param confirmListener 確認按鈕點擊回調
     */
    public void showConfirmDialog(String message,
                                  OnDialogConfirmListener confirmListener) {
        mDialogHelper.showConfirmDialog(message, confirmListener);
    }

    /**
     * 顯示確認彈窗
     *
     * @param message         提示訊息
     * @param confirmText     確認按鈕文字
     * @param cancelText      取消按鈕文字
     * @param confirmListener 確認按鈕點擊回調
     */
    public void showConfirmDialog(String message,
                                  String confirmText,
                                  String cancelText,
                                  OnDialogConfirmListener confirmListener) {
        mDialogHelper.showConfirmDialog(message, confirmText, cancelText, confirmListener);
    }

    /**
     * 顯示確認彈窗
     *
     * @param message         提示訊息
     * @param confirmText     確認按鈕文字
     * @param cancelText      取消按鈕文字
     * @param confirmListener 確認按鈕點擊的回調
     * @param cancelListener  取消按鈕點擊的回調
     */
    public void showConfirmDialog(String message,
                                  String confirmText,
                                  String cancelText,
                                  final OnDialogConfirmListener confirmListener,
                                  final OnDialogCancelListener cancelListener) {
        mDialogHelper.showConfirmDialog(message, confirmText, cancelText, confirmListener, cancelListener);
    }

    /**
     * 顯示確認彈窗
     *
     * @param message         提示訊息
     * @param confirmText     確認按鈕文字
     * @param cancelText      取消按鈕文字
     * @param confirmListener 確認按鈕點擊的回調
     * @param cancelListener  取消按鈕點擊的回調
     */
    public void showConfirmDialog(String message,
                                  String confirmText,
                                  String cancelText,
                                  final OnDialogConfirmListener confirmListener,
                                  final OnDialogCancelListener cancelListener,
                                  final DialogInterface.OnKeyListener keyListener) {
        mDialogHelper.showConfirmDialog(message, confirmText, cancelText, confirmListener, cancelListener, keyListener);
    }
    // endregion [ Confirm 視窗 ]
    // region [ 密碼輸入視窗 ]

    /**
     * 顯示密碼輸入彈窗
     *
     * @param messageId       標題文字
     * @param confirmListener 確認按鈕點擊回調
     */
    public void showPasswordVerifyDialog(@StringRes int messageId, OnPasswordVerifyDialogConfirmListener confirmListener) {
        mDialogHelper.showPasswordVerifyDialog(messageId, confirmListener);
    }

    /**
     * 顯示密碼輸入彈窗
     *
     * @param messageId       標題文字
     * @param confirmId       確認按鈕文字
     * @param cancelId        取消按鈕文字
     * @param confirmListener 確認按鈕點擊回調
     */
    public void showPasswordVerifyDialog(@StringRes int messageId,
                                         @StringRes int confirmId,
                                         @StringRes int cancelId,
                                         final OnPasswordVerifyDialogConfirmListener confirmListener) {
        mDialogHelper.showPasswordVerifyDialog(messageId, confirmId, cancelId, confirmListener);
    }

    /**
     * 顯示密碼輸入彈窗
     *
     * @param messageId       標題文字
     * @param confirmId       確認按鈕文字
     * @param cancelId        取消按鈕文字
     * @param confirmListener 確認按鈕點擊回調
     * @param cancelListener  取消按鈕點擊回調
     */
    public void showPasswordVerifyDialog(@StringRes int messageId,
                                         @StringRes int confirmId,
                                         @StringRes int cancelId,
                                         final OnPasswordVerifyDialogConfirmListener confirmListener,
                                         final OnDialogCancelListener cancelListener) {
        mDialogHelper.showPasswordVerifyDialog(messageId, confirmId, cancelId, confirmListener, cancelListener);
    }

    /**
     * 顯示密碼輸入彈窗
     *
     * @param messageId       標題文字
     * @param confirmId       確認按鈕文字
     * @param cancelId        取消按鈕文字
     * @param confirmListener 確認按鈕點擊回調
     * @param cancelListener  取消按鈕點擊回調
     * @param onKeyListener   監聽按鍵的回調
     */
    public void showPasswordVerifyDialog(@StringRes int messageId,
                                         @StringRes int confirmId,
                                         @StringRes int cancelId,
                                         final OnPasswordVerifyDialogConfirmListener confirmListener,
                                         final OnDialogCancelListener cancelListener,
                                         final DialogInterface.OnKeyListener onKeyListener) {
        mDialogHelper.showPasswordVerifyDialog(messageId, confirmId, cancelId, confirmListener, cancelListener, onKeyListener);
    }

    /**
     * 顯示密碼輸入彈窗
     *
     * @param header          標題文字
     * @param confirmListener 確認按鈕點擊回調
     */
    public void showPasswordVerifyDialog(String header, OnPasswordVerifyDialogConfirmListener confirmListener) {
        mDialogHelper.showPasswordVerifyDialog(header, confirmListener);
    }

    /**
     * 顯示密碼輸入彈窗
     *
     * @param header          標題文字
     * @param confirmText     確認按鈕文字
     * @param cancelText      取消按鈕文字
     * @param confirmListener 確認按鈕點擊回調
     */
    public void showPasswordVerifyDialog(String header,
                                         String confirmText,
                                         String cancelText,
                                         final OnPasswordVerifyDialogConfirmListener confirmListener) {
        mDialogHelper.showPasswordVerifyDialog(header, confirmText, cancelText, confirmListener);
    }

    /**
     * 顯示密碼輸入彈窗
     *
     * @param header          標題文字
     * @param confirmText     確認按鈕文字
     * @param cancelText      取消按鈕文字
     * @param confirmListener 確認按鈕點擊回調
     * @param cancelListener  取消按鈕點擊回調
     */
    public void showPasswordVerifyDialog(String header,
                                         String confirmText,
                                         String cancelText,
                                         final OnPasswordVerifyDialogConfirmListener confirmListener,
                                         final OnDialogCancelListener cancelListener) {
        mDialogHelper.showPasswordVerifyDialog(header, confirmText, cancelText, confirmListener, cancelListener);
    }

    /**
     * 顯示密碼輸入彈窗
     *
     * @param header          標題文字
     * @param confirmText     確認按鈕文字
     * @param cancelText      取消按鈕文字
     * @param confirmListener 確認按鈕點擊回調
     * @param cancelListener  取消按鈕點擊回調
     * @param onKeyListener   監聽按鍵的回調
     */
    public void showPasswordVerifyDialog(String header,
                                         String confirmText,
                                         String cancelText,
                                         final OnPasswordVerifyDialogConfirmListener confirmListener,
                                         final OnDialogCancelListener cancelListener,
                                         final DialogInterface.OnKeyListener onKeyListener) {
        mDialogHelper.showPasswordVerifyDialog(header, confirmText, cancelText, confirmListener, cancelListener, onKeyListener);
    }
    // endregion [ 密碼輸入視窗 ]
    // region [ 文字輸入視窗 ]

    /**
     * 顯示文字輸入彈窗
     *
     * @param messageId       標題文字
     * @param confirmListener 確認按鈕點擊回調
     */
    public void showTextInputDialog(@StringRes int messageId, OnTextInputDialogConfirmListener confirmListener) {
        mDialogHelper.showTextInputDialog(messageId, confirmListener);
    }

    /**
     * 顯示文字輸入彈窗
     *
     * @param messageId       標題文字
     * @param confirmId       確認按鈕文字
     * @param confirmListener 確認按鈕點擊回調
     */
    public void showTextInputDialog(@StringRes int messageId,
                                    @StringRes int confirmId,
                                    final OnTextInputDialogConfirmListener confirmListener) {
        mDialogHelper.showTextInputDialog(messageId, confirmId, confirmListener);
    }

    /**
     * 顯示文字輸入彈窗
     *
     * @param messageId       標題文字
     * @param confirmId       確認按鈕文字
     * @param confirmListener 確認按鈕點擊回調
     * @param onKeyListener   監聽按鍵的回調
     */
    public void showTextInputDialog(@StringRes int messageId,
                                    @StringRes int confirmId,
                                    final OnTextInputDialogConfirmListener confirmListener,
                                    final DialogInterface.OnKeyListener onKeyListener) {
        mDialogHelper.showTextInputDialog(messageId, confirmId, confirmListener, onKeyListener);
    }

    /**
     * 顯示文字輸入彈窗
     *
     * @param header          標題文字
     * @param confirmListener 確認按鈕點擊回調
     */
    public void showTextInputDialog(String header, OnTextInputDialogConfirmListener confirmListener) {
        mDialogHelper.showTextInputDialog(header, confirmListener);
    }

    /**
     * 顯示文字輸入彈窗
     *
     * @param header          標題文字
     * @param confirmText     確認按鈕文字
     * @param confirmListener 確認按鈕點擊回調
     */
    public void showTextInputDialog(String header,
                                    String confirmText,
                                    final OnTextInputDialogConfirmListener confirmListener) {
        mDialogHelper.showTextInputDialog(header, confirmText, confirmListener);
    }

    /**
     * 顯示文字輸入彈窗
     *
     * @param header          標題文字
     * @param confirmText     確認按鈕文字
     * @param confirmListener 確認按鈕點擊回調
     * @param onKeyListener   監聽按鍵的回調
     */
    public void showTextInputDialog(String header,
                                    String confirmText,
                                    final OnTextInputDialogConfirmListener confirmListener,
                                    final DialogInterface.OnKeyListener onKeyListener) {
        mDialogHelper.showTextInputDialog(header, confirmText, confirmListener, onKeyListener);
    }
    // endregion [ 文字輸入視窗 ]

    /**
     * @param dismissTime if input value is 0, default dismissTime is 200, else dismissTime is input value.
     */
    public void onDismissDialog(long dismissTime) {
        if (dismissTime == 0) {
            dismissTime = 200;
        }

        mHandler.postDelayed(() -> mDialogHelper.dismissDialog(), dismissTime);
    }

    @Override
    public void onDialogCancelListener(AlertDialog dialog) {
        // 空實現，讓子類做自己想做的事情
    }

    @Override
    public void onDialogKeyListener(AlertDialog dialog, int i, KeyEvent keyEvent) {
        // 空實現，讓子類做自己想做的事情
    }
}
