package com.regalscan.architecture.core.base.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.regalscan.architecture.core.R;
import com.trello.rxlifecycle4.components.support.RxFragment;

public class BaseFragment extends RxFragment {
    protected Activity mActivity;
    protected Fragment mFragment;
    protected Handler mHandler;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mActivity = requireActivity();
        mFragment = this;
        mHandler = new Handler();
    }

    /**
     * 往左邊滑出
     */
    protected void slideLeftOut() {
        mActivity.overridePendingTransition(R.anim.setup_pre_in, R.anim.setup_pre_out);
    }

    /**
     * 從右邊滑入
     */
    protected void slideRightIn() {
        mActivity.overridePendingTransition(R.anim.setup_next_in, R.anim.setup_next_out);
    }
}
