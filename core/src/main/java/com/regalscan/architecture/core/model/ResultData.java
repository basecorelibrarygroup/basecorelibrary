package com.regalscan.architecture.core.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ResultData implements Serializable {
    /**
     *
     */
    @SerializedName("Query1")
    private Object query1;
    /**
     *
     */
    @SerializedName("Query2")
    private Object query2;
    /**
     *
     */
    @SerializedName("Query3")
    private Object query3;
    /**
     *
     */
    @SerializedName("Query4")
    private Object query4;

    public Object getQuery1() {
        return query1;
    }

    public void setQuery1(Object query1) {
        this.query1 = query1;
    }

    public Object getQuery2() {
        return query2;
    }

    public void setQuery2(Object query2) {
        this.query2 = query2;
    }

    public Object getQuery3() {
        return query3;
    }

    public void setQuery3(Object query3) {
        this.query3 = query3;
    }

    public Object getQuery4() {
        return query4;
    }

    public void setQuery4(Object query4) {
        this.query4 = query4;
    }
}
