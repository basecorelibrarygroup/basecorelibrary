package com.regalscan.architecture.core.mvp.presenter;

import android.content.ContentResolver;

import androidx.annotation.NonNull;

import com.regalscan.architecture.core.mvp.model.BaseModel;
import com.regalscan.architecture.core.mvp.view.BaseView;
import com.trello.rxlifecycle4.LifecycleTransformer;
import com.trello.rxlifecycle4.android.ActivityEvent;
import com.trello.rxlifecycle4.android.FragmentEvent;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.core.FlowableTransformer;
import io.reactivex.rxjava3.schedulers.Schedulers;

/**
 * 基類 Presenter，做統一的處理
 */
public abstract class BasePresenter<M extends BaseModel<?>> {
    private BaseView mView;

    protected M model;

    public BasePresenter(@NonNull BaseView view, @NonNull ContentResolver resolver) {
        this.mView = view;
    }

    /**
     * 取消訂閱
     */
    public void unDisposable() {
        if (model != null) {
            model.unDisposable();
        }
    }

    /**
     * 取消訂閱並釋放資源
     */
    public void dispose() {
        if (mView != null) {
            mView = null;
        }

        if (model != null) {
            model.unDisposable();
        }
    }

    /**
     * 統一線程處理和綁定生命週期
     *
     * @param <E> E
     * @return Publisher<E>
     */
    public <E> FlowableTransformer<E, E> getScheduler() {
        return observable -> {
            // 最終用於訂閱的
            LifecycleTransformer<E> bindUntilEvent = null;
            if (mView instanceof com.trello.rxlifecycle4.components.RxActivity) {
                bindUntilEvent = ((com.trello.rxlifecycle4.components.RxActivity) mView).bindUntilEvent(ActivityEvent.DESTROY);
            }
            if (mView instanceof com.trello.rxlifecycle4.components.RxDialogFragment) {
                bindUntilEvent = ((com.trello.rxlifecycle4.components.RxDialogFragment) mView).bindUntilEvent(FragmentEvent.DESTROY);
            }
            if (mView instanceof com.trello.rxlifecycle4.components.RxFragment) {
                bindUntilEvent = ((com.trello.rxlifecycle4.components.RxFragment) mView).bindUntilEvent(FragmentEvent.DESTROY);
            }
            if (mView instanceof com.trello.rxlifecycle4.components.RxPreferenceFragment) {
                bindUntilEvent = ((com.trello.rxlifecycle4.components.RxPreferenceFragment) mView).bindUntilEvent(FragmentEvent.DESTROY);
            }

            if (mView instanceof com.trello.rxlifecycle4.components.support.RxAppCompatActivity) {
                bindUntilEvent = ((com.trello.rxlifecycle4.components.support.RxAppCompatActivity) mView).bindUntilEvent(ActivityEvent.DESTROY);
            }
            if (mView instanceof com.trello.rxlifecycle4.components.support.RxAppCompatDialogFragment) {
                bindUntilEvent = ((com.trello.rxlifecycle4.components.support.RxAppCompatDialogFragment) mView).bindUntilEvent(FragmentEvent.DESTROY);
            }
            if (mView instanceof com.trello.rxlifecycle4.components.support.RxDialogFragment) {
                bindUntilEvent = ((com.trello.rxlifecycle4.components.support.RxDialogFragment) mView).bindUntilEvent(FragmentEvent.DESTROY);
            }
            if (mView instanceof com.trello.rxlifecycle4.components.support.RxFragment) {
                bindUntilEvent = ((com.trello.rxlifecycle4.components.support.RxFragment) mView).bindUntilEvent(FragmentEvent.DESTROY);
            }
            if (mView instanceof com.trello.rxlifecycle4.components.support.RxFragmentActivity) {
                bindUntilEvent = ((com.trello.rxlifecycle4.components.support.RxFragmentActivity) mView).bindUntilEvent(ActivityEvent.DESTROY);
            }
            // 判定是相應的類型，進行綁定
            Flowable<E> flowable = observable
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread());

            if (bindUntilEvent != null) {
                // 綁定
                flowable = observable
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .compose(bindUntilEvent);
            }

            return flowable;
        };
    }
}
