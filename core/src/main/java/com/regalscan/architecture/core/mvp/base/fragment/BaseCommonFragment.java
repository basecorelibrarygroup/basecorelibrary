package com.regalscan.architecture.core.mvp.base.fragment;

import android.graphics.Color;

import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.navigation.Navigation;

import com.regalscan.architecture.core.R;
import com.regalscan.architecture.core.base.fragment.BaseDialogFragment;
import com.regalscan.architecture.core.mvp.presenter.BasePresenter;
import com.regalscan.architecture.core.mvp.view.BaseView;
import com.regalscan.architecture.core.net.exception.NetErrorException;

public abstract class BaseCommonFragment<P extends BasePresenter<?>> extends BaseDialogFragment implements BaseView {
    private P mPresenter;

    protected Toolbar mToolbar;
    /**
     * 是否初始化了 Toolbar
     */
    protected boolean isInitToolbar = false;

    /**
     * 創建 Presenter
     *
     * @return Presenter 的實例
     */
    protected abstract P createPresenter();

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        // 銷毀 取消訂閱
        if (mPresenter != null) {
//            mPresenter.unDisposable();
            mPresenter.dispose();
        }
    }

    /**
     * 底層獲取 Presenter
     *
     * @return P
     */
    protected synchronized P getPresenter() {
        if (mPresenter == null) {
            mPresenter = createPresenter();
        }
        return mPresenter;
    }

    @Override
    public void onLoading(@StringRes int resId) {
        showLoadingDialog(resId);
    }

    @Override
    public void onLoading(@StringRes int resId, boolean cancelable) {
        showLoadingDialog(resId, cancelable);
    }

    @Override
    public void onLoading(String message) {
        showLoadingDialog(message);
    }

    @Override
    public void onLoading(String message, boolean cancelable) {
        showLoadingDialog(message, cancelable);
    }

    // region [ 使用過時的 ProgressDialog ]

    @Override
    public void onLoadingOlder(int resId) {
        setProgressMessage(resId);
    }

    @Override
    public void onLoadingOlder(int resId, boolean cancelable) {
        setProgressMessage(resId, cancelable);
    }

    @Override
    public void onLoadingOlder(String message) {
        setProgressMessage(message);
    }

    @Override
    public void onLoadingOlder(String message, boolean cancelable) {
        setProgressMessage(message, cancelable);
    }

    // endregion [ 使用過時的 ProgressDialog ]

    @Override
    public void onMessage(@StringRes int resId) {
        showMessageDialog(resId);
    }

    @Override
    public void onMessage(String message) {
        showMessageDialog(message);
    }

    @Override
    public void onProgressMessage(@StringRes int resId) {
        setProgressMessage(resId);
    }

    @Override
    public void onProgressMessage(String message) {
        setProgressMessage(message);
    }

    @Override
    public void onWarning(@StringRes int resId) {
        showWarningDialog(resId);
    }

    @Override
    public void onWarning(String message) {
        showWarningDialog(message);
    }

    @Override
    public void onError(@StringRes int resId) {
        showErrorDialog(resId);
    }

    @Override
    public void onError(String message) {
        showErrorDialog(message);
    }

    @Override
    public void onError(Exception exception) {
        showErrorDialog(exception.getMessage());
    }

    @Override
    public void onError(NetErrorException exception) {
        showErrorDialog(exception.getMessage());
    }

    @Override
    public void onComplete() {
        // 請求完成，關閉 Dialog
        onDismissDialog(0);
    }

    @Override
    public void onDialogCancelListener(AlertDialog dialog) {
        super.onDialogCancelListener(dialog);
        // Dialog 取消，取消訂閱
        if (mPresenter != null) {
            mPresenter.unDisposable();
        }
    }

    protected void setToolbar() {
        setToolbar(R.id.base_toolbar);
    }

    protected void setToolbar(int resId) {
        setToolbar(resId, "");
    }

    protected void setToolbar(int resId, String title) {
        setToolbar(resId, title, -1);
    }

    protected void setToolbar(int resId, String title, int host) {
        mToolbar = requireActivity().findViewById(resId);
        if (mToolbar != null) {
            // 清除標題
            mToolbar.setTitle(title);
            // 設置為透明色
            mToolbar.setBackgroundColor(Color.TRANSPARENT);
            // 設置為全透明
            mToolbar.getBackground().setAlpha(0);

            ((AppCompatActivity) requireActivity()).setSupportActionBar(mToolbar);

            //子類中沒有設置過返回按鈕的情況下
            if (mToolbar.getNavigationIcon() == null) {
                // 設置返回按鈕
                mToolbar.setNavigationIcon(R.drawable.arrow_back_24);
            }

            if (host != -1) {
                mToolbar.setNavigationOnClickListener(view -> {
                    onBackPressed(host);
                });
            }

            isInitToolbar = true;
        }
    }

    protected void onBackPressed(int host) {
        Navigation.findNavController(requireActivity(), host).popBackStack();
        slideLeftOut();
    }
}
