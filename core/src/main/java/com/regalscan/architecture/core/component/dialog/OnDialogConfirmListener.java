package com.regalscan.architecture.core.component.dialog;

import androidx.appcompat.app.AlertDialog;

public interface OnDialogConfirmListener {
    /**
     * 確認按鈕點擊的回調
     *
     * @param dialog {@link AlertDialog}
     */
    void onDialogConfirmListener(AlertDialog dialog);
}
