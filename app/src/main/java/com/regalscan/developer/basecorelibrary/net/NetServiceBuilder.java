package com.regalscan.developer.basecorelibrary.net;

import com.regalscan.architecture.core.net.BaseApi;

public class NetServiceBuilder {
    private static volatile NetService mNetService;
    private static final Object mLock = new Object();

    public static NetService getService() {
        if (mNetService == null) {
            synchronized (mLock) {
                if (mNetService == null) {
                    mNetService = BaseApi.get(NetService.class);
                }
            }
        }

        return mNetService;
    }
}
