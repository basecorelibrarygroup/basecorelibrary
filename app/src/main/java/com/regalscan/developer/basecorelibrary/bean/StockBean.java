package com.regalscan.developer.basecorelibrary.bean;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;

import java.io.Serializable;

/**
 * 庫別 (CMSMC)
 */
public class StockBean implements Serializable {
    /**
     * 庫別代號
     */
    private String MC001;
    /**
     * 庫別名稱
     */
    private String MC002;
    /**
     * 廠別代號
     */
    private String MC003;
    /**
     * 庫別性質
     */
    private String MC004;
    /**
     * 確認時庫存量不足准許出庫
     */
    private String MC006;
    /**
     * 存檔時庫存量不足准許出庫
     */
    private String MC008;
    /**
     * 是否為冷凍庫
     */
    private String MC200;

    public StockBean() {
    }

    /**
     * @param MC001 庫別代號
     * @param MC002 庫別名稱
     * @param MC003 廠別代號
     * @param MC004 庫別性質
     * @param MC006 確認時庫存量不足准許出庫
     * @param MC008 存檔時庫存量不足准許出庫
     * @param MC200 是否為冷凍庫
     */
    public StockBean(String MC001, String MC002, String MC003, String MC004, String MC006, String MC008, String MC200) {
        this.MC001 = MC001;
        this.MC002 = MC002;
        this.MC003 = MC003;
        this.MC004 = MC004;
        this.MC006 = MC006;
        this.MC008 = MC008;
        this.MC200 = MC200;
    }

    public String getMC001() {
        return MC001;
    }

    public void setMC001(String MC001) {
        this.MC001 = MC001;
    }

    public String getMC002() {
        return MC002;
    }

    public void setMC002(String MC002) {
        this.MC002 = MC002;
    }

    public String getMC003() {
        return MC003;
    }

    public void setMC003(String MC003) {
        this.MC003 = MC003;
    }

    public String getMC004() {
        return MC004;
    }

    public void setMC004(String MC004) {
        this.MC004 = MC004;
    }

    public String getMC006() {
        return MC006;
    }

    public void setMC006(String MC006) {
        this.MC006 = MC006;
    }

    public String getMC008() {
        return MC008;
    }

    public void setMC008(String MC008) {
        this.MC008 = MC008;
    }

    public String getMC200() {
        return MC200;
    }

    public void setMC200(String MC200) {
        this.MC200 = MC200;
    }

    @NonNull
    @Override
    public String toString() {
        return MC002;
    }

    public static DiffUtil.ItemCallback<StockBean> itemCallback = new DiffUtil.ItemCallback<>() {
        @Override
        public boolean areItemsTheSame(@NonNull StockBean oldItem, @NonNull StockBean newItem) {
            return false;
        }

        @Override
        public boolean areContentsTheSame(@NonNull StockBean oldItem, @NonNull StockBean newItem) {
            return false;
        }
    };
}
