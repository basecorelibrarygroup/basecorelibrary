package com.regalscan.developer.basecorelibrary;

import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import com.regalscan.architecture.core.mvp.base.activity.BaseCommonActivity;
import com.regalscan.architecture.core.utils.NetworkUtils;
import com.regalscan.developer.basecorelibrary.bean.UserInfoBean;
import com.regalscan.platform.bean.AppBean;
import com.regalscan.platform.utils.UpdateUtils;
import com.regalscan.regalutilitylib.LogUtil;

import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends
//        AppCompatActivity {
        BaseCommonActivity<MainPresenter> implements MainContract.View {

    private TextView txt_View;
    private Button btn_Next;

    private ProgressBar pb_main;
    private ProgressBar pb_second;
    private TextView txt_main;
    private TextView txt_second;
    private int main_i = 0;
    private int second_i = 0;

    /**
     * 能使用的 Apps
     */
    private String[] canUseApps;
    /**
     * Apps 的排序清單
     */
    private String[] sortApps;
    /**
     * 是否不理會 update 的 flag
     */
    private boolean updatePass = false;
    /**
     * 判斷是否有連上網路
     */
    private boolean isOnline = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        isOnline = NetworkUtils.isOnline(mActivity);
        // 如果 updatePass 為 true 的話, 則無視網路識別, 直接 pass
        updatePass = updatePass || !isOnline;

        View view1 = LayoutInflater.from(mActivity).inflate(R.layout.layout_dialog_downloading, null);
        pb_main = view1.findViewById(R.id.pb_dialog_downloading_main);
        pb_second = view1.findViewById(R.id.pb_dialog_downloading_second);
        txt_main = view1.findViewById(R.id.txt_dialog_downloading_main);
        txt_second = view1.findViewById(R.id.txt_dialog_downloading_second);

        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity, com.regalscan.architecture.core.R.style.AppAlertDialogStyle);
        builder.setView(view1);
        builder.setCancelable(true);

        txt_View = findViewById(R.id.txt_view);
        btn_Next = findViewById(R.id.btn_1);
        btn_Next.setOnClickListener(view -> {
            UpdateUtils.checkVersion(mActivity, canUseApps, sortApps, updatePass, isOnline, !isOnline,
                    new UpdateUtils.UpdateListener() {
                        @Override
                        public void onPreExecute() {
                            runOnUiThread(() -> {
                                onLoadingOlder("更新中", true);
                            });
                        }

                        @Override
                        public void onSuccess(ArrayList<AppBean> list) {
                            runOnUiThread(() -> {
                                onLoadingOlder("更新完成", true);
                                onDismissDialog(0);
                            });
                        }

                        @Override
                        public void onDownloading(String message) {
                            runOnUiThread(() -> {
                                onLoadingOlder(message, true);
                                mHandler.postDelayed(() -> {
                                    onError(new Exception("測試彈出錯誤視窗"));
                                }, 5000);
                            });
                        }

                        @Override
                        public void onFail(boolean needFinish, String errMsg) {
                            runOnUiThread(() -> {
                                String eM = "";
                                try {
                                    eM = new JSONObject(errMsg).getString("Message");
                                } catch (Exception e) {
                                    LogUtil.writeException(e.toString());
                                }

                                if (TextUtils.isEmpty(eM)) {
                                    eM = errMsg;
                                }

                                onError(eM);
                            });
                        }

                        @Override
                        public void onFinishActivity() {
                            finish();
                        }
                    });
        });


        findViewById(R.id.btn_2).setOnClickListener(view -> {
//            Intent intent = new Intent(this,  MenuActivity.class);
//            startActivity(intent);
//            finish();
            showLoadingDialog("測試中...", true);
        });

        new Handler().postDelayed(() -> {
//            getPresenter().FetchEnvPath();
//            getPresenter().FetchPreCreateData(mActivity, new QueryModel.Builder()
//                    .setDBName("SMARTDEMO")
//                    .setUserId("DS")
//                    .create());
            getPresenter().VerifyUserInfo(mActivity, new UserInfoBean(
                    "DS",
                    "DS",
                    true
            ));
        }, 1000);
    }

    @Override
    protected MainPresenter createPresenter() {
        return new MainPresenter(this, getContentResolver());
    }

    @Override
    public void ShowEnvPath(String path) {
//        showMessageDialog(path);
        txt_View.setText(path);
    }

    @Override
    public void showVer(String ver) {
        showMessageDialog(ver);
    }
}