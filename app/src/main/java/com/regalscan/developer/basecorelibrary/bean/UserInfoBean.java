package com.regalscan.developer.basecorelibrary.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UserInfoBean implements Serializable {
    @SerializedName("UserId")
    private String userId;
    @SerializedName("UserName")
    private String userName;
    @SerializedName("Password")
    private String password;
    @SerializedName("IsPasswordEnable")
    private boolean isPasswordEnable;
    @SerializedName("Today")
    private String today;

    public UserInfoBean() {
    }

    public UserInfoBean(String userId, String password, boolean isPasswordEnable) {
        this.userId = userId;
        this.password = password;
        this.isPasswordEnable = isPasswordEnable;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isPasswordEnable() {
        return isPasswordEnable;
    }

    public void setPasswordEnable(boolean passwordEnable) {
        this.isPasswordEnable = passwordEnable;
    }

    public String getToday() {
        return today;
    }

    public void setToday(String today) {
        this.today = today;
    }
}
