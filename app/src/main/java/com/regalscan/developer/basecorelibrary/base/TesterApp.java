package com.regalscan.developer.basecorelibrary.base;

import android.app.Application;
import android.os.Environment;

import com.regalscan.architecture.core.constant.BaseObj;
import com.regalscan.architecture.core.net.BaseApi;
import com.regalscan.architecture.core.net.NetConfig;
import com.regalscan.architecture.core.net.interceptor.HostSelectionInterceptor;
import com.regalscan.platform.global.Ini;

import okhttp3.Interceptor;

public class TesterApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        /**
         * AAR 需要使用以下資料庫參數
         * UpdateURL        更新網址
         * UPDATE_MODE      Y=啟用 ; N=不啟用
         * TRANSFER_MODE    Y=啟用 ; N=不啟用
         */

        //region   AAR 使用的 DataBase 路徑 以及是否開啟Crash攔截
        String DBPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath();
        boolean showLogOutInMenu = false;
        //endregion

        //-------以下僅供Login專案使用，作業模組專案請勿使用以下程式碼-------//
        //region   設定Log使用的參數
        Ini.LogParm parm = new Ini.LogParm();
        parm.LogOnOff = true;
        parm.needUncaughtException = true;
        parm.Delay = 14;
        //endregion
        Ini.init(this, DBPath, showLogOutInMenu, parm);

        String hostPackageName = "com.regalscan.p22037.login";
        String res[] = hostPackageName.split("\\.");
        String group = res[0] + "." + res[1] + "." + res[2];

        Ini.HOSTPACKAGENAME = hostPackageName;
        Ini.GROUPPACKAGE = group;
        Ini.DBName = res[2] + "_Host.db3";
        //----------------------------------------------------------//

        BaseApi.registerConfig(new NetConfig() {
            @Override
            public String BaseUrl() {

                BaseObj.URL_WEBAPI = "http://192.168.6.182/RegalWebApi/";

                return BaseObj.URL_WEBAPI;
            }

            @Override
            public Interceptor[] Interceptors() {
                Interceptor[] mInterceptors = new Interceptor[1];
                mInterceptors[0] = new HostSelectionInterceptor();
                return mInterceptors;
            }

            @Override
            public long ConnectTimeoutMills() {
                return 30 * 1000;
            }

            @Override
            public long ReadTimeoutMills() {
                return 30 * 1000;
            }

            @Override
            public boolean isLogEnable() {
                return true;
            }
        });
    }
}
