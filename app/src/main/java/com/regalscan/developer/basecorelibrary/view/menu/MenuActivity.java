package com.regalscan.developer.basecorelibrary.view.menu;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;

import com.google.android.material.navigation.NavigationView;
import com.regalscan.architecture.core.mvp.base.activity.BaseCommonActivity;
import com.regalscan.developer.basecorelibrary.R;
import com.regalscan.developer.basecorelibrary.view.MainActivity2;
import com.regalscan.developer.basecorelibrary.view.MainActivity3;

public class MenuActivity extends BaseCommonActivity<MenuPresenter> implements MenuContract.View {

    private DrawerLayout mDrawer;
    private FrameLayout mFrame;
    private NavigationView mNavigation;

    private Intent intent = new Intent();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_menu);

//        isInitToolbar = true;

//        mDrawer = (DrawerLayout) getLayoutInflater().inflate(R.layout.nav_drawer, findViewById(R.id.layout_drawer_root));
//        mFrame = mDrawer.findViewById(R.id.nav_content);
//        mNavigation = mDrawer.findViewById(R.id.nav_view);
//        getLayoutInflater().inflate(R.layout.activity_menu, mFrame, true);
//        setContentView(mDrawer);

        mDrawer = findViewById(R.id.layout_drawer_root);
        mNavigation = findViewById(R.id.nav_view);
    }

    @Override
    protected MenuPresenter createPresenter() {
        return new MenuPresenter(this, getContentResolver());
    }

    @Override
    protected void onStart() {
        super.onStart();
        try {
            initComponent();
            initView();
        } catch (Exception e) {
            onError(e.getMessage());
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void initComponent() {

    }

    private void initView() {

        mNavigation.setNavigationItemSelectedListener(item -> {

            int itemId = item.getItemId();
            if (itemId == R.id.menu_action_setting) {
                intent = new Intent(mActivity, MainActivity2.class);
            } else if (itemId == R.id.menu_action_about) {
                intent = new Intent(mActivity, MainActivity3.class);
            } else if (itemId == R.id.menu_action_logout) {
                showConfirmDialog("敘述A", getString(com.regalscan.architecture.core.R.string.core_btn_confirm), getString(com.regalscan.architecture.core.R.string.core_btn_cancel),
                        dialog -> finish());
            }

            mHandler.postDelayed(() -> {
                startActivity(intent);
            }, 100);

            mDrawer.closeDrawer(GravityCompat.START);
            return false;
        });

//        setToolbar(binding.toolbar.getId(), getString(R.string.setting_toolbar_text));

        mToolbar = findViewById(R.id.toolbar);
        mToolbar.setTitle("");
        mToolbar.setBackgroundColor(Color.TRANSPARENT);
        mToolbar.getBackground().setAlpha(0);
        setSupportActionBar(mToolbar);

        mToolbar.setNavigationOnClickListener(view -> mDrawer.closeDrawer(GravityCompat.START));
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(mActivity, mDrawer, mToolbar, com.regalscan.architecture.core.R.string.navigation_drawer_open, com.regalscan.architecture.core.R.string.navigation_drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };
        mDrawer.addDrawerListener(toggle);
        toggle.syncState();



//        binding.menuRvList.setLayoutManager(new LinearLayoutManager(mActivity));
        RecyclerView rvList = findViewById(R.id.menu_rv_list);
        rvList.setLayoutManager(new LinearLayoutManager(mActivity));
    }

}