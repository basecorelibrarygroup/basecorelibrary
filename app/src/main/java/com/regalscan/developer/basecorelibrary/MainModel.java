package com.regalscan.developer.basecorelibrary;

import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;

import androidx.annotation.NonNull;

import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.regalscan.architecture.core.model.NetResultModel;
import com.regalscan.architecture.core.net.ApiSubscriber;
import com.regalscan.developer.basecorelibrary.bean.CountryBean;
import com.regalscan.developer.basecorelibrary.bean.PreCreateBean;
import com.regalscan.developer.basecorelibrary.bean.StockBean;
import com.regalscan.developer.basecorelibrary.bean.TypeBean;
import com.regalscan.developer.basecorelibrary.bean.UserInfoBean;
import com.regalscan.developer.basecorelibrary.bean.VendorBean;
import com.regalscan.developer.basecorelibrary.net.NetServiceBuilder;
import com.regalscan.developer.basecorelibrary.net.model.QueryModel;

import org.reactivestreams.Publisher;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.functions.Function;

public class MainModel extends MainContract.Model {

    public MainModel(@NonNull MainContract.Presenter presenter, @NonNull ContentResolver resolver) {
        super(presenter, resolver);
    }

    @Override
    public void FetchEnvPath() {
        unDisposable();

        mDisposable = Flowable.timer(10, TimeUnit.MILLISECONDS)
                .flatMap(new Function<Long, Publisher<String>>() {
                    @Override
                    public Publisher<String> apply(Long aLong) throws Throwable {
                        if (BuildConfig.DEBUG) {
                            return Flowable.error(new Exception("測試錯誤"));
                        }
                        return Flowable.just(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString());
                    }
                })
                .compose(presenter.getScheduler())
                .subscribeWith(new ApiSubscriber<>(presenter.view) {
                    @Override
                    public void onNext(String s) {
                        super.onNext(s);
                        presenter.ShowEnvPath(s);
                    }

//                    @Override
//                    public void onError(Throwable t) {
//                        super.onError(t);
//                    }
                });
    }

    @Override
    public void FetchPreCreateData(QueryModel query) {
        unDisposable();

        mDisposable = Flowable.timer(10, TimeUnit.MILLISECONDS)
                .flatMap((Function<Long, Publisher<NetResultModel>>) aLong -> {
//                    if (BuildConfig.DEBUG) {
//                        NetResultModel model = new NetResultModel();
//                        model.setStatusCode(200);
//                        model.setMessage("");
//                        model.setJsonInfo();
//                        model.setResult(true);
//                        return Flowable.just(model);
//                    }
//                    try {
                    return NetServiceBuilder
                            .getService()
                            .FetchPreCreateData(
                                    createRequestBody(query)
                            );
//                    } catch (Exception e) {
//                        return Flowable.error(e);
//                    }
                })
                .compose(presenter.getScheduler())
                .subscribeWith(new ApiSubscriber<>(presenter.view) {
                    @Override
                    public void onNext(NetResultModel model) {
                        super.onNext(model);

                        try {
                            PreCreateBean bean = new PreCreateBean();

                            // region [ 單別資料 ]
                            JsonElement element = gson.toJsonTree(model.getData().getQuery1());
                            List<TypeBean> typeList = gson.fromJson(element, new TypeToken<>() {
                            }.getType());
                            bean.setType(typeList);
                            // endregion [ 單別資料 ]
                            // region [ 國別資料 ]
                            element = gson.toJsonTree(model.getData().getQuery2());
                            List<CountryBean> countryList = gson.fromJson(element, new TypeToken<>() {
                            }.getType());
                            bean.setCountry(countryList);
                            // endregion [ 國別資料 ]
                            // region [ 供應商資料 ]
                            element = gson.toJsonTree(model.getData().getQuery3());
                            List<VendorBean> vendorList = gson.fromJson(element, new TypeToken<>() {
                            }.getType());
                            bean.setVendor(vendorList);
                            // endregion [ 供應商資料 ]
                            // region [ 庫別資料 ]
                            element = gson.toJsonTree(model.getData().getQuery4());
                            List<StockBean> stockList = gson.fromJson(element, new TypeToken<>() {
                            }.getType());
                            bean.setStock(stockList);
                            // endregion [ 庫別資料 ]

//                            presenter.ShowPreCreateData(bean);

                        } catch (Exception e) {
                            presenter.view.onError(e);
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        super.onError(t);
                    }
                });
    }

    @Override
    public void VerifyUserInfo(Context context, UserInfoBean bean) {
        unDisposable();

        mDisposable = Flowable.timer(10, TimeUnit.MILLISECONDS)
                .flatMap((Function<Long, Publisher<NetResultModel>>) aLong -> NetServiceBuilder.getService()
                        .VerifyLogin(createRequestBody(bean)))
//                        .WebApiVer())
                .compose(presenter.getScheduler())
                .subscribeWith(new ApiSubscriber<>(presenter.view) {
                    @Override
                    public void onNext(NetResultModel model) {
                        super.onNext(model);
                        try {
                            JsonElement element = gson.toJsonTree(model.getJsonInfo());
//                            String ver = gson.fromJson(element, new TypeToken<String>(){}.getType());
//                            presenter.showVer(ver);
                            UserInfoBean bean = gson.fromJson(element, new TypeToken<UserInfoBean>() {
                            }.getType());

//                            settingHelper.setUserId(bean.getUserId());
//                            settingHelper.setUserName(bean.getUserName());
//                            settingHelper.setPassword(bean.getPassword());
//                            settingHelper.setToken(model.getToken());

                            element = gson.toJsonTree(model.getData().getQuery1());
//                            List<CompanyInfoBean> data = gson.fromJson(element, new TypeToken<>() {
//                            }.getType());

//                            SharedPreferences sp = context.getSharedPreferences(ShareObj.Settings, Context.MODE_PRIVATE);
//                            sp.edit().putString(ShareObj.CompanyList, data.toString()).apply();

//                            presenter.ShowVerifyResult(context, bean);
                        } catch (Exception e) {

                            onError(e);
                        }
                    }
                });
    }
}
