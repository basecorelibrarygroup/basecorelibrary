package com.regalscan.developer.basecorelibrary.bean;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;

import java.io.Serializable;

/**
 * 進貨單別 (CMSMQ)
 */
public class TypeBean implements Serializable {
    /**
     * 單別代號
     */
    private String MQ001;
    /**
     * 單別名稱
     */
    private String MQ002;

    public TypeBean() {
    }

    /**
     * @param MQ001 單別代號
     * @param MQ002 單別名稱
     */
    public TypeBean(String MQ001, String MQ002) {
        this.MQ001 = MQ001;
        this.MQ002 = MQ002;
    }

    public String getMQ001() {
        return MQ001;
    }

    public void setMQ001(String MQ001) {
        this.MQ001 = MQ001;
    }

    public String getMQ002() {
        return MQ002;
    }

    public void setMQ002(String MQ002) {
        this.MQ002 = MQ002;
    }

    @NonNull
    @Override
    public String toString() {
        return String.format("%s-%s", MQ001, MQ002);
    }

    public static DiffUtil.ItemCallback<TypeBean> itemCallback = new DiffUtil.ItemCallback<TypeBean>() {
        @Override
        public boolean areItemsTheSame(@NonNull TypeBean oldItem, @NonNull TypeBean newItem) {
            return false;
        }

        @Override
        public boolean areContentsTheSame(@NonNull TypeBean oldItem, @NonNull TypeBean newItem) {
            return false;
        }
    };
}
