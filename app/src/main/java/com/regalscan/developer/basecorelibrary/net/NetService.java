package com.regalscan.developer.basecorelibrary.net;

import com.regalscan.architecture.core.constant.BaseObj;
import com.regalscan.architecture.core.model.NetResultModel;

import io.reactivex.rxjava3.core.Flowable;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface NetService {

    @Headers({BaseObj.DOMAIN_NAME_HEADER + BaseObj.DOMAIN_NAME_WEBAPI})
    @POST("api/Verify/Login")
    Flowable<NetResultModel> VerifyLogin(@Body RequestBody body);

//    @Headers({BaseObj.DOMAIN_NAME_HEADER + BaseObj.DOMAIN_NAME_WEBAPI})
    @POST("api/PURMI09/FetchPreCreateData")
    Flowable<NetResultModel> FetchPreCreateData(@Body RequestBody body);

//    @Headers({BaseObj.DOMAIN_NAME_HEADER + BaseObj.DOMAIN_NAME_WEBAPI})
    @POST("api/PURMI09/SearchProductData")
    Flowable<NetResultModel> SearchProductData(@Body RequestBody body);

    @GET("api/Basic/WebApiVer")
    Flowable<NetResultModel> WebApiVer();

//    @Headers({})
//    @POST("api/")
//    Flowable<NetResultModel> FetchStockData(@Body RequestBody body);
//
//    @Headers({})
//    @POST("api/")
//    Flowable<NetResultModel> CheckBarcode(@Body RequestBody body);
//
//    @Headers({})
//    @POST("api/")
//    Flowable<NetResultModel> CheckLot(@Body RequestBody body);
//
//    @Headers({})
//    @POST("api/")
//    Flowable<NetResultModel> CheckStockQty(@Body RequestBody body);

//    @Headers({BaseObj.DOMAIN_NAME_HEADER + BaseObj.DOMAIN_NAME_WEBAPI})
    @POST("api/PURMI09Upload/SaveToBTS")
    Flowable<NetResultModel> SaveToBTS(@Body RequestBody body);
}
