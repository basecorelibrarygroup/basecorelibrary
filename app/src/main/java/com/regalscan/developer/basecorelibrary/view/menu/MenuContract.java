package com.regalscan.developer.basecorelibrary.view.menu;

import android.content.ContentResolver;

import androidx.annotation.NonNull;

import com.regalscan.architecture.core.mvp.model.BaseModel;
import com.regalscan.architecture.core.mvp.presenter.BasePresenter;
import com.regalscan.architecture.core.mvp.view.BaseView;

public interface MenuContract {
    interface View extends BaseView {

    }

    abstract class Presenter extends BasePresenter<Model> {
        protected View view;

        public Presenter(@NonNull View view, @NonNull ContentResolver resolver) {
            super(view, resolver);
            this.view = view;
        }


    }

    abstract class Model extends BaseModel<Presenter> {

        public Model(@NonNull Presenter presenter, @NonNull ContentResolver resolver) {
            super(presenter, resolver);
        }
    }
}
