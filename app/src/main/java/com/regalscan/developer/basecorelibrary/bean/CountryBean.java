package com.regalscan.developer.basecorelibrary.bean;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;

import java.io.Serializable;

/**
 * 國別 (CMSMR)
 * 地區別 要查 MR001='3'
 * 國家別 要查 Mr001='4'
 */
public class CountryBean implements Serializable {
    /**
     * 分類方式
     * 1:通路
     * 2:型態
     * 3:地區
     * 4:國家
     * 5:路線
     * 6:其他
     * 7:抽成
     * 8:活動
     * 9:廠商分類
     */
    private String MR001;
    /**
     * 分類代號
     */
    private String MR002;
    /**
     * 分類簡稱
     */
    private String MR003;
    /**
     * 分類全名
     */
    private String MR004;

    public CountryBean() {
    }

    /**
     * @param MR001 分類方式
     * @param MR002 分類代號
     * @param MR003 分類簡稱
     * @param MR004 分類全名
     */
    public CountryBean(String MR001, String MR002, String MR003, String MR004) {
        this.MR001 = MR001;
        this.MR002 = MR002;
        this.MR003 = MR003;
        this.MR004 = MR004;
    }

    public String getMR001() {
        return MR001;
    }

    public void setMR001(String MR001) {
        this.MR001 = MR001;
    }

    public String getMR002() {
        return MR002;
    }

    public void setMR002(String MR002) {
        this.MR002 = MR002;
    }

    public String getMR003() {
        return MR003;
    }

    public void setMR003(String MR003) {
        this.MR003 = MR003;
    }

    public String getMR004() {
        return MR004;
    }

    public void setMR004(String MR004) {
        this.MR004 = MR004;
    }

    @NonNull
    @Override
    public String toString() {
        return MR003;
    }

    public static DiffUtil.ItemCallback<CountryBean> itemCallback = new DiffUtil.ItemCallback<>() {
        @Override
        public boolean areItemsTheSame(@NonNull CountryBean oldItem, @NonNull CountryBean newItem) {
            return false;
        }

        @Override
        public boolean areContentsTheSame(@NonNull CountryBean oldItem, @NonNull CountryBean newItem) {
            return false;
        }
    };
}
