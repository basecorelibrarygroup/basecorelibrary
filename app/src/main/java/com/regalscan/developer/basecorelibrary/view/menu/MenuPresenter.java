package com.regalscan.developer.basecorelibrary.view.menu;

import android.content.ContentResolver;

import androidx.annotation.NonNull;

public class MenuPresenter extends MenuContract.Presenter {
    private MenuContract.Model model;

    public MenuPresenter(@NonNull MenuContract.View view, @NonNull ContentResolver resolver) {
        super(view, resolver);
    }
}
