package com.regalscan.developer.basecorelibrary.view.menu;

import android.content.ContentResolver;

import androidx.annotation.NonNull;

public class MenuModel extends MenuContract.Model {

    public MenuModel(@NonNull MenuContract.Presenter presenter, @NonNull ContentResolver resolver) {
        super(presenter, resolver);
    }
}
