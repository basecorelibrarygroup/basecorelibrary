package com.regalscan.developer.basecorelibrary.net.model;

public class QueryModel {
    private final String dbName;

    private final String userId;

    private final String stock;

    private final String barcode;

//    private final String lot;

//    private final String qty;

    private QueryModel(Builder builder) {
        this.dbName = builder.dbName;
        this.userId = builder.userId;
        this.stock = builder.stock;
        this.barcode = builder.barcode;
//        this.lot = builder.lot;
//        this.qty = builder.qty;
    }

    /**
     * Access field {@link QueryModel#dbName}
     */
    public String getDBName() {
        return dbName;
    }

    /**
     * Access field {@link QueryModel#userId}
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Access Field {@link QueryModel#stock}
     */
    public String getStock() {
        return stock;
    }

    /**
     * Access field {@link QueryModel#barcode}
     */
    public String getBarcode() {
        return barcode;
    }

    /**
     * Access field {@link QueryModel#lot}
     */
//    public String getLot() {
//        return lot;
//    }

    /**
     * Access field {@link QueryModel#qty}
     */
//    public String getQty() {
//        return qty;
//    }

    public static class Builder {
        private String dbName;

        private String userId;

        private String stock;

        private String barcode;

//        private String lot;

//        private String qty;

        public Builder setDBName(String dbName) {
            this.dbName = dbName;
            return this;
        }

        public Builder setUserId(String userId) {
            this.userId = userId;
            return this;
        }

        public Builder setStock(String stock) {
            this.stock = stock;
            return this;
        }

        public Builder setBarcode(String barcode) {
            this.barcode = barcode;
            return this;
        }

//        public Builder setLot(String lot) {
//            this.lot = lot;
//            return this;
//        }
//
//        public Builder setQty(String qty) {
//            this.qty = qty;
//            return this;
//        }

        public QueryModel create() {
            return new QueryModel(this);
        }
    }
}
