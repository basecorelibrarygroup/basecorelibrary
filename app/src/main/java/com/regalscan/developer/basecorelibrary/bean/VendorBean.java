package com.regalscan.developer.basecorelibrary.bean;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;

import java.io.Serializable;

/**
 * 供應商 (PURMA)
 */
public class VendorBean implements Serializable {
    /**
     * 廠商代號
     */
    private String MA001;
    /**
     * 廠商簡稱
     */
    private String MA002;
    /**
     * 廠商全名
     */
    private String MA003;
    /**
     * 國家別
     */
    private String MA006;
    /**
     * 地區別
     */
    private String MA007;
    /**
     * 國別
     */
    private String MA200;
    /**
     * 品號條碼起始位置
     */
    private String MA201;
    /**
     * 品號條碼截止位置
     */
    private String MA202;
    /**
     * 包裝日期條碼起始位置
     */
    private String MA203;
    /**
     * 包裝日期條碼起截止位置
     */
    private String MA204;
    /**
     * 淨重條碼起始位置
     */
    private String MA205;
    /**
     * 淨重條碼截止位置
     */
    private String MA206;
    /**
     * 序號條碼起始位置
     */
    private String MA207;
    /**
     * 序號條碼截止位置
     */
    private String MA208;

    /**
     *
     */
    public VendorBean() {
    }

    /**
     * @param MA001 廠商代號
     * @param MA002 廠商簡稱
     * @param MA003 廠商全名
     * @param MA006 國家別
     * @param MA007 地區別
     * @param MA200 國別
     * @param MA201 品號條碼起始位置
     * @param MA202 品號條碼截止位置
     * @param MA203 包裝日期條碼起始位置
     * @param MA204 包裝日期條碼起截止位置
     * @param MA205 淨重條碼起始位置
     * @param MA206 淨重條碼截止位置
     * @param MA207 序號條碼起始位置
     * @param MA208 序號條碼截止位置
     */
    public VendorBean(String MA001, String MA002, String MA003, String MA006, String MA007, String MA200, String MA201, String MA202, String MA203, String MA204, String MA205, String MA206, String MA207, String MA208) {
        this.MA001 = MA001;
        this.MA002 = MA002;
        this.MA003 = MA003;
        this.MA006 = MA006;
        this.MA007 = MA007;
        this.MA200 = MA200;
        this.MA201 = MA201;
        this.MA202 = MA202;
        this.MA203 = MA203;
        this.MA204 = MA204;
        this.MA205 = MA205;
        this.MA206 = MA206;
        this.MA207 = MA207;
        this.MA208 = MA208;
    }

    public String getMA001() {
        return MA001;
    }

    public void setMA001(String MA001) {
        this.MA001 = MA001;
    }

    public String getMA002() {
        return MA002;
    }

    public void setMA002(String MA002) {
        this.MA002 = MA002;
    }

    public String getMA003() {
        return MA003;
    }

    public void setMA003(String MA003) {
        this.MA003 = MA003;
    }

    public String getMA006() {
        return MA006;
    }

    public void setMA006(String MA006) {
        this.MA006 = MA006;
    }

    public String getMA007() {
        return MA007;
    }

    public void setMA007(String MA007) {
        this.MA007 = MA007;
    }

    public String getMA200() {
        return MA200;
    }

    public void setMA200(String MA200) {
        this.MA200 = MA200;
    }

    public String getMA201() {
        return MA201;
    }

    public void setMA201(String MA201) {
        this.MA201 = MA201;
    }

    public String getMA202() {
        return MA202;
    }

    public void setMA202(String MA202) {
        this.MA202 = MA202;
    }

    public String getMA203() {
        return MA203;
    }

    public void setMA203(String MA203) {
        this.MA203 = MA203;
    }

    public String getMA204() {
        return MA204;
    }

    public void setMA204(String MA204) {
        this.MA204 = MA204;
    }

    public String getMA205() {
        return MA205;
    }

    public void setMA205(String MA205) {
        this.MA205 = MA205;
    }

    public String getMA206() {
        return MA206;
    }

    public void setMA206(String MA206) {
        this.MA206 = MA206;
    }

    public String getMA207() {
        return MA207;
    }

    public void setMA207(String MA207) {
        this.MA207 = MA207;
    }

    public String getMA208() {
        return MA208;
    }

    public void setMA208(String MA208) {
        this.MA208 = MA208;
    }

    @NonNull
    @Override
    public String toString() {
        return MA002;
    }

    public static DiffUtil.ItemCallback<VendorBean> itemCallback = new DiffUtil.ItemCallback<>() {
        @Override
        public boolean areItemsTheSame(@NonNull VendorBean oldItem, @NonNull VendorBean newItem) {
            return false;
        }

        @Override
        public boolean areContentsTheSame(@NonNull VendorBean oldItem, @NonNull VendorBean newItem) {
            return false;
        }
    };
}
