package com.regalscan.developer.basecorelibrary;

import android.content.ContentResolver;
import android.content.Context;

import androidx.annotation.NonNull;

import com.regalscan.architecture.core.mvp.model.BaseModel;
import com.regalscan.architecture.core.mvp.presenter.BasePresenter;
import com.regalscan.architecture.core.mvp.view.BaseView;
import com.regalscan.developer.basecorelibrary.bean.PreCreateBean;
import com.regalscan.developer.basecorelibrary.bean.UserInfoBean;
import com.regalscan.developer.basecorelibrary.net.model.QueryModel;

public interface MainContract {
    interface View extends BaseView {

        void ShowEnvPath(String path);

        void showVer(String ver);
    }

    abstract class Presenter extends BasePresenter<MainContract.Model> {
        protected View view;

        public Presenter(@NonNull View view, @NonNull ContentResolver resolver) {
            super(view, resolver);
            this.view = view;
        }

        public abstract void FetchEnvPath();

        public abstract void ShowEnvPath(String path);

        public abstract void FetchPreCreateData(Context context, QueryModel query);

//        public abstract void ShowPreCreateData(PreCreateBean bean);

        public abstract void VerifyUserInfo(Context context, UserInfoBean bean);

        public abstract void showVer(String ver);
    }

    abstract class Model extends BaseModel<MainContract.Presenter> {

        public Model(@NonNull MainContract.Presenter presenter, @NonNull ContentResolver resolver) {
            super(presenter, resolver);
        }

        public abstract void FetchEnvPath();

        public abstract void FetchPreCreateData(QueryModel query);

        public abstract void VerifyUserInfo(Context context, UserInfoBean bean);
    }
}
