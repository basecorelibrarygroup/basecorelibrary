package com.regalscan.developer.basecorelibrary;

import android.content.ContentResolver;
import android.content.Context;

import androidx.annotation.NonNull;

import com.regalscan.developer.basecorelibrary.bean.PreCreateBean;
import com.regalscan.developer.basecorelibrary.bean.UserInfoBean;
import com.regalscan.developer.basecorelibrary.net.model.QueryModel;

public class MainPresenter extends MainContract.Presenter {

    public MainPresenter(@NonNull MainContract.View view, @NonNull ContentResolver resolver) {
        super(view, resolver);
        try {
            model = new MainModel(this, resolver);
        } catch (Exception e) {

            view.onError(e);
        }
    }

    @Override
    public void FetchEnvPath() {
        model.FetchEnvPath();
    }

    @Override
    public void ShowEnvPath(String path) {
        view.ShowEnvPath(path);
    }

    @Override
    public void FetchPreCreateData(Context context, QueryModel query) {
        model.FetchPreCreateData(query);
    }

    @Override
    public void VerifyUserInfo(Context context, UserInfoBean bean) {
        model.VerifyUserInfo(context, bean);
    }

    @Override
    public void showVer(String ver) {
        if(view == null) {
            return;
        }

        view.showVer(ver);
    }
}
