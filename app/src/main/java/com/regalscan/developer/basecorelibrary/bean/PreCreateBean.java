package com.regalscan.developer.basecorelibrary.bean;

import java.io.Serializable;
import java.util.List;

public class PreCreateBean implements Serializable {
    private List<TypeBean> type;
    private List<CountryBean> country;
    private List<VendorBean> vendor;
    private List<StockBean> stock;

    public List<TypeBean> getType() {
        return type;
    }

    public void setType(List<TypeBean> type) {
        this.type = type;
    }

    public List<CountryBean> getCountry() {
        return country;
    }

    public void setCountry(List<CountryBean> country) {
        this.country = country;
    }

    public List<VendorBean> getVendor() {
        return vendor;
    }

    public void setVendor(List<VendorBean> vendor) {
        this.vendor = vendor;
    }

    public List<StockBean> getStock() {
        return stock;
    }

    public void setStock(List<StockBean> stock) {
        this.stock = stock;
    }
}
